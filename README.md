# README #

This repository contains the projects related to the Taquito language:

* A main project wich contains the grammar definition in antlr and which generates and tests the antlr lexer and parser.
* A Java project that contains a JetBrains plugin to provide a taquito editor
* A project/folder that contains syntax highlighter configuration files for text editors (e.g. Notepad++, Sublime, etc.)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact