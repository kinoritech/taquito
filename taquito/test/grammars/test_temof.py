from unittest import TestCase
import io

import pytest
from antlr4 import *
from antlr4.error.ErrorListener import *

from taquito.grammars.TaquitoLexer import TaquitoLexer
from taquito.grammars.TaquitoParser import TaquitoParser
from taquito.grammars.TaquitoListener import TaquitoListener


class TaquitoErrorListener(ErrorListener):
    def __init__(self, output):
        self.output = output
        self._symbol = ''

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        self.output.write(msg)
        self._symbol = offendingSymbol.text
        stack = recognizer.getRuleInvocationStack()
        stack.reverse()
        print("rule stack: {}".format(str(stack)))
        print("line {} : {} at {} : {}".format(str(line),
                                               str(column),
                                               str(offendingSymbol).replace(" ", u'\u23B5'),
                                               msg.replace(" ", u'\u23B5')))
    @property
    def symbol(self):
        return self._symbol


class TaquitoParserTests(TestCase):

    def setup(self, text):
        lexer = TaquitoLexer(InputStream(text))
        stream = CommonTokenStream(lexer)

        # print out the token parsing
        stream.fill()
        print("TOKENS")
        for token in stream.tokens:
            if token.text != '<EOF>':
                type_name = TaquitoParser.symbolicNames[token.type]
                tabs = 5 - len(type_name) // 4
                sep = "\t" * tabs
                print("    %s%s%s" % (type_name, sep,
                                      token.text.replace(" ", u'\u23B5').replace("\n", u'\u2936')))
        parser = TaquitoParser(stream)

        self.output = io.StringIO()
        self.error = io.StringIO()

        parser.removeErrorListeners()
        self.errorListener = TaquitoErrorListener(self.error)
        parser.addErrorListener(self.errorListener)
        return parser

    def test_valid_package(self):
        parser = self.setup("package org.example.library")
        parser.file_input()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enum_expr_stmt_full(self):
        parser = self.setup("Mystery=0")
        parser.t_enum_expr_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enum_expr_stmt_no_value(self):
        parser = self.setup("Mystery")
        parser.t_enum_expr_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_simple_enum_stmt(self):
        parser = self.setup("Mystery = 0; pass; Crime\n")
        parser.t_simple_enum_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enumdef_suite_simple(self):
        parser = self.setup("Mystery = 0; pass; Crime\n")
        parser.t_enumdef_suite()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enumdef_suite_group(self):
        parser = self.setup("\n"
                            "    Mystery = 0\n"
                            "    pass; Crime;\n"
                            "    Biography = 2\n"
                            "")
        parser.t_enumdef_suite()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enumdef_pass(self):
        parser = self.setup("enum BookCategory:\n"
                            "    pass\n"
                            "")
        parser.t_enumdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_enumdef_simple(self):
        parser = self.setup("enum BookCategory:\n"
                            "    Mystery\n"
                            "    ScienceFiction\n"
                            "    Biography\n"
                            "")
        parser.t_enumdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_extends_stmt_one(self):
        parser = self.setup("(Parent)")
        parser.t_extends_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_extends_stmt_many(self):
        parser = self.setup("(Parent, Other, ThatOne)")
        parser.t_extends_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multi_range_zero_to_upper(self):
        parser = self.setup("0..3")
        parser.t_multi_range()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multi_range_lower_to_upper(self):
        parser = self.setup("2..15")
        parser.t_multi_range()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multi_range_no_lower(self):
        parser = self.setup("2")
        parser.t_multi_range()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multi_range_unlimited(self):
        parser = self.setup("2..*")
        parser.t_multi_range()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multi_range_unbound(self):
        parser = self.setup("*")
        parser.t_multi_range()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multiplicity_range(self):
        parser = self.setup("[0..1]")
        parser.t_multiplicity()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_multiplicity_single(self):
        parser = self.setup("[1]")
        parser.t_multiplicity()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_attribute_no_mult(self):
        parser = self.setup("String name")
        parser.attr_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_attribute_default_value(self):
        parser = self.setup("String name = 'John Doe'")
        parser.attr_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_attribute_mult(self):
        parser = self.setup("Integer [0..5] grades")
        parser.attr_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_containment_no_mult_no_opp(self):
        parser = self.setup("contains Book books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_container_no_mult_no_opp(self):
        parser = self.setup("container Book books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_refers_no_mult_no_opp(self):
        parser = self.setup("refers Book books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_containment_mult_no_opp(self):
        parser = self.setup("contains Book[*] books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_container_mult_no_opp(self):
        parser = self.setup("container Book[1..20] books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_refers_mult_no_opp(self):
        parser = self.setup("refers Book[0..1] books")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_containment_mult_opp(self):
        parser = self.setup("contains Book[*] books opposite Library")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_container_mult_opp(self):
        parser = self.setup("container Book[1..20] books opposite Library")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_ref_refers_mult_opp(self):
        parser = self.setup("refers Book[0..1] books opposite Library")
        parser.ref_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_derived_feat_stmt(self):
        parser = self.setup("derived Level level get:\n"
                            "    level = self.volume/3\n"
                            "    return level")
        parser.derived_feat_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    """ Associations"""
    def test_t_associationiation_empty(self):
        parser = self.setup("association AhasB: pass\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_empty_stmts(self):
        parser = self.setup("association AhasB:\n"
                            "    pass\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_abstract(self):
        parser = self.setup("abstract association AhasB:\n"
                            "    pass\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_extends_association(self):
        parser = self.setup("association AhasB (ZhasX):\n"
                            "    pass\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_extends_many(self):
        parser = self.setup("association AhasB (ZhasX, ZownsB):\n"
                            "    pass\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_simple_stmt_member_end(self):
        parser = self.setup("classA.property; classB.property\n")
        parser.t_assoc_simple_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_simple_stmt_one_owned_end(self):
        parser = self.setup("classA.property; SomeClass property\n")
        parser.t_assoc_simple_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_simple_no_owned_end(self):
        parser = self.setup("association AhasB: classA.property; classB.property\n")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_compound_one_owned_end(self):
        parser = self.setup("association AhasB:\n"
                            "    classA.property\n"
                            "    classB property\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_compound_only_one(self):
        parser = self.setup("association AhasB:\n"
                            "    classA.property\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_association_compound_more_than_two(self):
        parser = self.setup("association AhasB:\n"
                            "    classA.property\n"
                            "    classB property\n"
                            "    classC.property\n"
                            "")
        parser.t_assocdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    """ Class """
    def test_t_class_abstract(self):
        parser = self.setup("abstract class Library:\n"
                            "    pass\n"
                            "")
        parser.t_classdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_empty(self):
        parser = self.setup("class Library:\n"
                            "    pass\n"
                            "")
        parser.t_classdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_extends_class(self):
        parser = self.setup("class Library (Place):\n"
                            "    pass\n"
                            "")
        parser.t_classdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_extends_many(self):
        parser = self.setup("class Library (Place, Building):\n"
                            "    pass\n"
                            "")
        parser.t_classdef()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_simple_stmt_with_attributes(self):
        parser = self.setup("String title; int pages\n")
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_simple_stmt_with_attr_and_references(self):
        parser = self.setup("String title; int pages; container Library library opposite books; refers Writer[*] authors opposite books\n")
        parser.t_class_simple_stmt()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_suite_with_simple_stmt(self):
        parser = self.setup("\n"
                            "    String title; container Library library opposite books\n"
                            "")
        parser.t_class_suite()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_suite_with_funcdef(self):
        parser = self.setup("\n"
                            "    def someOp(self, param):\n"
                            "        return a\n"
                            "")
        parser.t_class_suite()
        self.assertEqual(len(self.errorListener.symbol), 0)

    def test_t_class_complete(self):
        parser = self.setup("class Library:\n"
                            "    String name = 'District Library'\n"
                            "    contains Book[] books opposite library\n"
                            "    contains Writer[] authors\n"
                            "    def getBook(self, title):\n"
                            "        return self.books.select_one(lambda x: x.title == name)")
        #tree = parser.t_classdef()
        #listener = TemofListener()
        #walker = ParseTreeWalker()
        #walker.walk(listener, tree)
        parser.t_classdef()
        self.assertEqual(len(self.errorListener.symbol), 0)


class TaquitoGrammarTests(TestCase):

    def setup(self, path):
        input = FileStream(path)
        lexer = TaquitoLexer(input)
        stream = CommonTokenStream(lexer)

        # print out the token parsing
        stream.fill()
        print("TOKENS")
        for token in stream.tokens:
            if token.text != '<EOF>':
                token_type = TaquitoParser.symbolicNames[token.type]
                tabs = 5 - len(token_type) // 4
                sep = "\t" * tabs
                if token_type == 'NEWLINE':
                    print(u"    {}{}\u2936".format(token_type, sep))
                else:
                    print(u"    {}{}{}".format(token_type,
                                                sep,
                                                token.text.replace(" ", u'\u23B5')))
        parser = TaquitoParser(stream)

        self.output = io.StringIO()
        self.error = io.StringIO()

        parser.removeErrorListeners()
        self.errorListener = TaquitoErrorListener(self.error)
        parser.addErrorListener(self.errorListener)
        return parser

    def test_grammar(self):
        parser = self.setup("test_grammar.takto")
        parser.file_input()
        self.assertEqual(len(self.errorListener.symbol), 0)



