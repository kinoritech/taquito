grammar Taquito;
import Python3;

file_input
 : NEWLINE* t_packdef ( NEWLINE | t_package_stmt )* EOF
 ;

t_packdef
 : PACKAGE dotted_name AT uri=STRING_LITERAL AS prefix=STRING_LITERAL
 ;

t_package_stmt
 : t_classdef
 | t_assocdef
 | t_enumdef
 ;

t_classdef
 : ABSTRACT? CLASS NAME (t_extends_stmt)? COLON t_class_suite
 ;

t_class_suite
 : t_class_simple_stmt
 | NEWLINE INDENT t_class_stmt+ DEDENT
 ;

t_class_simple_stmt
 : t_class_small_stmt ( SEMI_COLON t_class_small_stmt )* SEMI_COLON? NEWLINE
 ;

t_class_small_stmt
 : feat_stmt
 | pass_stmt
 ;

t_class_stmt
 : t_class_simple_stmt
 | t_class_compound_stmt
 ;

t_class_compound_stmt
 : derived_feat
 | funcdef
 ;

feat_stmt
 : ref_stmt
 | attr_stmt
 ;

derived_feat
 : DERIVED feat_stmt COLON derived_feat_suite
 ;

derived_feat_suite
 : derived_feat_simple_stmt
 | NEWLINE INDENT derived_feat_stmt+ DEDENT
 ;

derived_feat_stmt
 : derived_feat_simple_stmt
 | derived_feat_compound_stmt
 ;

derived_feat_simple_stmt
 : derived_feat_small_stmt ( SEMI_COLON derived_feat_small_stmt )* SEMI_COLON? NEWLINE
 ;

derived_feat_small_stmt
 : pass_stmt
 ;

derived_feat_compound_stmt
 : derived_feat_xet_stmt
 ;

derived_feat_get_stmt
 : DEF GET COLON suite
 ;

derived_feat_set_stmt
 : DEF SET '(' tfpdef ')' COLON suite
 ;

derived_feat_xet_stmt
 : derived_feat_get_stmt
 | derived_feat_set_stmt
 ;

t_multiplicity
 : OPEN_BRACK t_multi_range CLOSE_BRACK
 | OPEN_BRACK  CLOSE_BRACK
 ;

t_multi_range
 : (LOWER_RANGE)? (number | STAR)
 ;

ref_stmt
 : (REFERS | CONTAINS | CONTAINER) NAME (t_multiplicity)? NAME (OPPOSITE NAME)?
 ;

attr_stmt
 : NAME t_multiplicity? NAME ('=' test)?
 ;

t_extends_stmt
 : '(' NAME (COMMA NAME)* ')'
 ;

t_enum_stmt
 : t_simple_enum_stmt
 ;

t_simple_enum_stmt
 : t_small_enum_stmt ( SEMI_COLON t_small_enum_stmt )* SEMI_COLON? NEWLINE
 ;

t_small_enum_stmt
 : pass_stmt
 | t_enum_expr_stmt
 ;

t_enum_expr_stmt
 : NAME (t_enum_value )?
 ;

t_enum_value
 : ASSIGN number
 ;

t_enumdef_suite
 : t_simple_enum_stmt
 | NEWLINE INDENT t_enum_stmt+ DEDENT
 ;

t_enumdef
 : ENUM NAME COLON t_enumdef_suite
 ;

t_assocdef
 : ABSTRACT? ASSOCIATION NAME (t_extends_stmt)? COLON t_assoc_suite
 ;

t_assoc_suite
 : t_assoc_simple_stmt
 | NEWLINE INDENT t_assoc_stmt* DEDENT
 ;

t_assoc_simple_stmt
 : t_assoc_small_stmt ( SEMI_COLON t_assoc_small_stmt )* SEMI_COLON? NEWLINE
 ;

t_assoc_small_stmt
 : assoc_classifier_end
 | feat_stmt
 | pass_stmt
 ;

t_assoc_stmt
 : t_assoc_simple_stmt
 | t_assoc_compound_stmt
 ;

t_assoc_compound_stmt
 : derived_feat
 ;

assoc_classifier_end
 : END dotted_name       // Allow classes in other packages to be accessed
 ;

PACKAGE: 'package';
ABSTRACT: 'abstract';
EXTENDS: 'extends';
CONTAINS: 'contains';
CONTAINER: 'container';
OPPOSITE: 'opposite';
REFERS: 'refers';
END: 'end';
DERIVED: 'derived';
GET: 'get';
SET: 'set';
ENUM: 'enum';
ASSOCIATION: 'association';
RANGE : '..';
LOWER_RANGE
 : INT_PART '..'
 ;