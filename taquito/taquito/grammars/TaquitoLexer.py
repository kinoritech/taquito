# Generated from /Users/horacio/Git/taquito/taquito/antlr/Taquito.g4 by ANTLR 4.7
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from antlr4.Token import CommonToken
import re
import importlib

# Allow languages to extend the lexer and parser, by loading the parser dinamically
module_path = __name__[:-5]
language_name = __name__.split('.')[-1]
language_name = language_name[:-5]  # Remove Lexer from name
LanguageParser = getattr(importlib.import_module('{}Parser'.format(module_path)), '{}Parser'.format(language_name))


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2m")
        buf.write("\u03cd\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4L\t")
        buf.write("L\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT\4U\t")
        buf.write("U\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4")
        buf.write("^\t^\4_\t_\4`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4")
        buf.write("g\tg\4h\th\4i\ti\4j\tj\4k\tk\4l\tl\4m\tm\4n\tn\4o\to\4")
        buf.write("p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4w\tw\4x\tx\4")
        buf.write("y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080")
        buf.write("\t\u0080\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083")
        buf.write("\4\u0084\t\u0084\4\u0085\t\u0085\4\u0086\t\u0086\4\u0087")
        buf.write("\t\u0087\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3")
        buf.write("\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f")
        buf.write("\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\20")
        buf.write("\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26")
        buf.write("\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35")
        buf.write("\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3 \3!\3!")
        buf.write("\3!\3!\3!\3!\3!\3!\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#")
        buf.write("\3#\3#\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3&\3&\3&\3&\3\'\3")
        buf.write("\'\3\'\3\'\3(\3(\3(\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3+\3")
        buf.write("+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3.\3")
        buf.write(".\3.\3.\3/\3/\3/\3/\3/\3\60\3\60\3\60\3\60\3\60\3\60\3")
        buf.write("\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62")
        buf.write("\3\62\5\62\u022d\n\62\3\62\3\62\5\62\u0231\n\62\3\62\5")
        buf.write("\62\u0234\n\62\5\62\u0236\n\62\3\62\3\62\3\63\3\63\7\63")
        buf.write("\u023c\n\63\f\63\16\63\u023f\13\63\3\64\5\64\u0242\n\64")
        buf.write("\3\64\5\64\u0245\n\64\3\64\3\64\5\64\u0249\n\64\3\65\3")
        buf.write("\65\5\65\u024d\n\65\3\65\3\65\5\65\u0251\n\65\3\66\3\66")
        buf.write("\7\66\u0255\n\66\f\66\16\66\u0258\13\66\3\66\6\66\u025b")
        buf.write("\n\66\r\66\16\66\u025c\5\66\u025f\n\66\3\67\3\67\3\67")
        buf.write("\6\67\u0264\n\67\r\67\16\67\u0265\38\38\38\68\u026b\n")
        buf.write("8\r8\168\u026c\39\39\39\69\u0272\n9\r9\169\u0273\3:\3")
        buf.write(":\5:\u0278\n:\3;\3;\5;\u027c\n;\3;\3;\3<\3<\3=\3=\3=\3")
        buf.write("=\3>\3>\3?\3?\3?\3@\3@\3@\3A\3A\3B\3B\3C\3C\3D\3D\3D\3")
        buf.write("E\3E\3F\3F\3F\3G\3G\3G\3H\3H\3I\3I\3J\3J\3K\3K\3K\3L\3")
        buf.write("L\3L\3M\3M\3N\3N\3O\3O\3P\3P\3Q\3Q\3Q\3R\3R\3S\3S\3S\3")
        buf.write("T\3T\3T\3U\3U\3V\3V\3W\3W\3W\3X\3X\3X\3Y\3Y\3Y\3Z\3Z\3")
        buf.write("Z\3[\3[\3[\3\\\3\\\3]\3]\3]\3^\3^\3^\3_\3_\3_\3`\3`\3")
        buf.write("`\3a\3a\3a\3b\3b\3b\3c\3c\3c\3d\3d\3d\3e\3e\3e\3f\3f\3")
        buf.write("f\3g\3g\3g\3g\3h\3h\3h\3h\3i\3i\3i\3i\3j\3j\3j\3j\3k\3")
        buf.write("k\3k\5k\u0304\nk\3k\3k\3l\3l\3m\3m\3m\7m\u030d\nm\fm\16")
        buf.write("m\u0310\13m\3m\3m\3m\3m\7m\u0316\nm\fm\16m\u0319\13m\3")
        buf.write("m\5m\u031c\nm\3n\3n\3n\3n\3n\7n\u0323\nn\fn\16n\u0326")
        buf.write("\13n\3n\3n\3n\3n\3n\3n\3n\3n\7n\u0330\nn\fn\16n\u0333")
        buf.write("\13n\3n\3n\3n\5n\u0338\nn\3o\3o\5o\u033c\no\3p\3p\3q\3")
        buf.write("q\3q\3r\3r\3s\3s\3t\3t\3u\3u\3v\3v\3w\5w\u034e\nw\3w\3")
        buf.write("w\3w\3w\5w\u0354\nw\3x\3x\5x\u0358\nx\3x\3x\3y\6y\u035d")
        buf.write("\ny\ry\16y\u035e\3z\3z\6z\u0363\nz\rz\16z\u0364\3{\3{")
        buf.write("\5{\u0369\n{\3{\6{\u036c\n{\r{\16{\u036d\3|\3|\3|\7|\u0373")
        buf.write("\n|\f|\16|\u0376\13|\3|\3|\3|\3|\7|\u037c\n|\f|\16|\u037f")
        buf.write("\13|\3|\5|\u0382\n|\3}\3}\3}\3}\3}\7}\u0389\n}\f}\16}")
        buf.write("\u038c\13}\3}\3}\3}\3}\3}\3}\3}\3}\7}\u0396\n}\f}\16}")
        buf.write("\u0399\13}\3}\3}\3}\5}\u039e\n}\3~\3~\5~\u03a2\n~\3\177")
        buf.write("\5\177\u03a5\n\177\3\u0080\5\u0080\u03a8\n\u0080\3\u0081")
        buf.write("\5\u0081\u03ab\n\u0081\3\u0082\3\u0082\3\u0082\3\u0083")
        buf.write("\6\u0083\u03b1\n\u0083\r\u0083\16\u0083\u03b2\3\u0084")
        buf.write("\3\u0084\7\u0084\u03b7\n\u0084\f\u0084\16\u0084\u03ba")
        buf.write("\13\u0084\3\u0085\3\u0085\5\u0085\u03be\n\u0085\3\u0085")
        buf.write("\5\u0085\u03c1\n\u0085\3\u0085\3\u0085\5\u0085\u03c5\n")
        buf.write("\u0085\3\u0086\5\u0086\u03c8\n\u0086\3\u0087\3\u0087\5")
        buf.write("\u0087\u03cc\n\u0087\6\u0324\u0331\u038a\u0397\2\u0088")
        buf.write("\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31")
        buf.write("\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31")
        buf.write("\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O")
        buf.write(")Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\67m8o9q:s;")
        buf.write("u<w=y>{?}@\177A\u0081B\u0083C\u0085D\u0087E\u0089F\u008b")
        buf.write("G\u008dH\u008fI\u0091J\u0093K\u0095L\u0097M\u0099N\u009b")
        buf.write("O\u009dP\u009fQ\u00a1R\u00a3S\u00a5T\u00a7U\u00a9V\u00ab")
        buf.write("W\u00adX\u00afY\u00b1Z\u00b3[\u00b5\\\u00b7]\u00b9^\u00bb")
        buf.write("_\u00bd`\u00bfa\u00c1b\u00c3c\u00c5d\u00c7e\u00c9f\u00cb")
        buf.write("g\u00cdh\u00cfi\u00d1j\u00d3k\u00d5l\u00d7m\u00d9\2\u00db")
        buf.write("\2\u00dd\2\u00df\2\u00e1\2\u00e3\2\u00e5\2\u00e7\2\u00e9")
        buf.write("\2\u00eb\2\u00ed\2\u00ef\2\u00f1\2\u00f3\2\u00f5\2\u00f7")
        buf.write("\2\u00f9\2\u00fb\2\u00fd\2\u00ff\2\u0101\2\u0103\2\u0105")
        buf.write("\2\u0107\2\u0109\2\u010b\2\u010d\2\3\2\32\4\2WWww\4\2")
        buf.write("TTtt\4\2DDdd\4\2QQqq\4\2ZZzz\4\2LLll\6\2\f\f\17\17))^")
        buf.write("^\6\2\f\f\17\17$$^^\3\2^^\3\2\63;\3\2\62;\3\2\629\5\2")
        buf.write("\62;CHch\3\2\62\63\4\2GGgg\4\2--//\7\2\2\13\r\16\20(*")
        buf.write("]_\u0081\7\2\2\13\r\16\20#%]_\u0081\4\2\2]_\u0081\3\2")
        buf.write("\2\u0081\4\2\13\13\"\"\4\2\f\f\17\17\u0129\2C\\aac|\u00ac")
        buf.write("\u00ac\u00b7\u00b7\u00bc\u00bc\u00c2\u00d8\u00da\u00f8")
        buf.write("\u00fa\u0243\u0252\u02c3\u02c8\u02d3\u02e2\u02e6\u02f0")
        buf.write("\u02f0\u037c\u037c\u0388\u0388\u038a\u038c\u038e\u038e")
        buf.write("\u0390\u03a3\u03a5\u03d0\u03d2\u03f7\u03f9\u0483\u048c")
        buf.write("\u04d0\u04d2\u04fb\u0502\u0511\u0533\u0558\u055b\u055b")
        buf.write("\u0563\u0589\u05d2\u05ec\u05f2\u05f4\u0623\u063c\u0642")
        buf.write("\u064c\u0670\u0671\u0673\u06d5\u06d7\u06d7\u06e7\u06e8")
        buf.write("\u06f0\u06f1\u06fc\u06fe\u0701\u0701\u0712\u0712\u0714")
        buf.write("\u0731\u074f\u076f\u0782\u07a7\u07b3\u07b3\u0906\u093b")
        buf.write("\u093f\u093f\u0952\u0952\u095a\u0963\u097f\u097f\u0987")
        buf.write("\u098e\u0991\u0992\u0995\u09aa\u09ac\u09b2\u09b4\u09b4")
        buf.write("\u09b8\u09bb\u09bf\u09bf\u09d0\u09d0\u09de\u09df\u09e1")
        buf.write("\u09e3\u09f2\u09f3\u0a07\u0a0c\u0a11\u0a12\u0a15\u0a2a")
        buf.write("\u0a2c\u0a32\u0a34\u0a35\u0a37\u0a38\u0a3a\u0a3b\u0a5b")
        buf.write("\u0a5e\u0a60\u0a60\u0a74\u0a76\u0a87\u0a8f\u0a91\u0a93")
        buf.write("\u0a95\u0aaa\u0aac\u0ab2\u0ab4\u0ab5\u0ab7\u0abb\u0abf")
        buf.write("\u0abf\u0ad2\u0ad2\u0ae2\u0ae3\u0b07\u0b0e\u0b11\u0b12")
        buf.write("\u0b15\u0b2a\u0b2c\u0b32\u0b34\u0b35\u0b37\u0b3b\u0b3f")
        buf.write("\u0b3f\u0b5e\u0b5f\u0b61\u0b63\u0b73\u0b73\u0b85\u0b85")
        buf.write("\u0b87\u0b8c\u0b90\u0b92\u0b94\u0b97\u0b9b\u0b9c\u0b9e")
        buf.write("\u0b9e\u0ba0\u0ba1\u0ba5\u0ba6\u0baa\u0bac\u0bb0\u0bbb")
        buf.write("\u0c07\u0c0e\u0c10\u0c12\u0c14\u0c2a\u0c2c\u0c35\u0c37")
        buf.write("\u0c3b\u0c62\u0c63\u0c87\u0c8e\u0c90\u0c92\u0c94\u0caa")
        buf.write("\u0cac\u0cb5\u0cb7\u0cbb\u0cbf\u0cbf\u0ce0\u0ce0\u0ce2")
        buf.write("\u0ce3\u0d07\u0d0e\u0d10\u0d12\u0d14\u0d2a\u0d2c\u0d3b")
        buf.write("\u0d62\u0d63\u0d87\u0d98\u0d9c\u0db3\u0db5\u0dbd\u0dbf")
        buf.write("\u0dbf\u0dc2\u0dc8\u0e03\u0e32\u0e34\u0e35\u0e42\u0e48")
        buf.write("\u0e83\u0e84\u0e86\u0e86\u0e89\u0e8a\u0e8c\u0e8c\u0e8f")
        buf.write("\u0e8f\u0e96\u0e99\u0e9b\u0ea1\u0ea3\u0ea5\u0ea7\u0ea7")
        buf.write("\u0ea9\u0ea9\u0eac\u0ead\u0eaf\u0eb2\u0eb4\u0eb5\u0ebf")
        buf.write("\u0ebf\u0ec2\u0ec6\u0ec8\u0ec8\u0ede\u0edf\u0f02\u0f02")
        buf.write("\u0f42\u0f49\u0f4b\u0f6c\u0f8a\u0f8d\u1002\u1023\u1025")
        buf.write("\u1029\u102b\u102c\u1052\u1057\u10a2\u10c7\u10d2\u10fc")
        buf.write("\u10fe\u10fe\u1102\u115b\u1161\u11a4\u11aa\u11fb\u1202")
        buf.write("\u124a\u124c\u124f\u1252\u1258\u125a\u125a\u125c\u125f")
        buf.write("\u1262\u128a\u128c\u128f\u1292\u12b2\u12b4\u12b7\u12ba")
        buf.write("\u12c0\u12c2\u12c2\u12c4\u12c7\u12ca\u12d8\u12da\u1312")
        buf.write("\u1314\u1317\u131a\u135c\u1382\u1391\u13a2\u13f6\u1403")
        buf.write("\u166e\u1671\u1678\u1683\u169c\u16a2\u16ec\u16f0\u16f2")
        buf.write("\u1702\u170e\u1710\u1713\u1722\u1733\u1742\u1753\u1762")
        buf.write("\u176e\u1770\u1772\u1782\u17b5\u17d9\u17d9\u17de\u17de")
        buf.write("\u1822\u1879\u1882\u18aa\u1902\u191e\u1952\u196f\u1972")
        buf.write("\u1976\u1982\u19ab\u19c3\u19c9\u1a02\u1a18\u1d02\u1dc1")
        buf.write("\u1e02\u1e9d\u1ea2\u1efb\u1f02\u1f17\u1f1a\u1f1f\u1f22")
        buf.write("\u1f47\u1f4a\u1f4f\u1f52\u1f59\u1f5b\u1f5b\u1f5d\u1f5d")
        buf.write("\u1f5f\u1f5f\u1f61\u1f7f\u1f82\u1fb6\u1fb8\u1fbe\u1fc0")
        buf.write("\u1fc0\u1fc4\u1fc6\u1fc8\u1fce\u1fd2\u1fd5\u1fd8\u1fdd")
        buf.write("\u1fe2\u1fee\u1ff4\u1ff6\u1ff8\u1ffe\u2073\u2073\u2081")
        buf.write("\u2081\u2092\u2096\u2104\u2104\u2109\u2109\u210c\u2115")
        buf.write("\u2117\u2117\u211a\u211f\u2126\u2126\u2128\u2128\u212a")
        buf.write("\u212a\u212c\u2133\u2135\u213b\u213e\u2141\u2147\u214b")
        buf.write("\u2162\u2185\u2c02\u2c30\u2c32\u2c60\u2c82\u2ce6\u2d02")
        buf.write("\u2d27\u2d32\u2d67\u2d71\u2d71\u2d82\u2d98\u2da2\u2da8")
        buf.write("\u2daa\u2db0\u2db2\u2db8\u2dba\u2dc0\u2dc2\u2dc8\u2dca")
        buf.write("\u2dd0\u2dd2\u2dd8\u2dda\u2de0\u3007\u3009\u3023\u302b")
        buf.write("\u3033\u3037\u303a\u303e\u3043\u3098\u309d\u30a1\u30a3")
        buf.write("\u30fc\u30fe\u3101\u3107\u312e\u3133\u3190\u31a2\u31b9")
        buf.write("\u31f2\u3201\u3402\u4db7\u4e02\u9fbd\ua002\ua48e\ua802")
        buf.write("\ua803\ua805\ua807\ua809\ua80c\ua80e\ua824\uac02\ud7a5")
        buf.write("\uf902\ufa2f\ufa32\ufa6c\ufa72\ufadb\ufb02\ufb08\ufb15")
        buf.write("\ufb19\ufb1f\ufb1f\ufb21\ufb2a\ufb2c\ufb38\ufb3a\ufb3e")
        buf.write("\ufb40\ufb40\ufb42\ufb43\ufb45\ufb46\ufb48\ufbb3\ufbd5")
        buf.write("\ufd3f\ufd52\ufd91\ufd94\ufdc9\ufdf2\ufdfd\ufe72\ufe76")
        buf.write("\ufe78\ufefe\uff23\uff3c\uff43\uff5c\uff68\uffc0\uffc4")
        buf.write("\uffc9\uffcc\uffd1\uffd4\uffd9\uffdc\uffde\u0096\2\62")
        buf.write(";\u0302\u0371\u0485\u0488\u0593\u05bb\u05bd\u05bf\u05c1")
        buf.write("\u05c1\u05c3\u05c4\u05c6\u05c7\u05c9\u05c9\u0612\u0617")
        buf.write("\u064d\u0660\u0662\u066b\u0672\u0672\u06d8\u06de\u06e1")
        buf.write("\u06e6\u06e9\u06ea\u06ec\u06ef\u06f2\u06fb\u0713\u0713")
        buf.write("\u0732\u074c\u07a8\u07b2\u0903\u0905\u093e\u093e\u0940")
        buf.write("\u094f\u0953\u0956\u0964\u0965\u0968\u0971\u0983\u0985")
        buf.write("\u09be\u09be\u09c0\u09c6\u09c9\u09ca\u09cd\u09cf\u09d9")
        buf.write("\u09d9\u09e4\u09e5\u09e8\u09f1\u0a03\u0a05\u0a3e\u0a3e")
        buf.write("\u0a40\u0a44\u0a49\u0a4a\u0a4d\u0a4f\u0a68\u0a73\u0a83")
        buf.write("\u0a85\u0abe\u0abe\u0ac0\u0ac7\u0ac9\u0acb\u0acd\u0acf")
        buf.write("\u0ae4\u0ae5\u0ae8\u0af1\u0b03\u0b05\u0b3e\u0b3e\u0b40")
        buf.write("\u0b45\u0b49\u0b4a\u0b4d\u0b4f\u0b58\u0b59\u0b68\u0b71")
        buf.write("\u0b84\u0b84\u0bc0\u0bc4\u0bc8\u0bca\u0bcc\u0bcf\u0bd9")
        buf.write("\u0bd9\u0be8\u0bf1\u0c03\u0c05\u0c40\u0c46\u0c48\u0c4a")
        buf.write("\u0c4c\u0c4f\u0c57\u0c58\u0c68\u0c71\u0c84\u0c85\u0cbe")
        buf.write("\u0cbe\u0cc0\u0cc6\u0cc8\u0cca\u0ccc\u0ccf\u0cd7\u0cd8")
        buf.write("\u0ce8\u0cf1\u0d04\u0d05\u0d40\u0d45\u0d48\u0d4a\u0d4c")
        buf.write("\u0d4f\u0d59\u0d59\u0d68\u0d71\u0d84\u0d85\u0dcc\u0dcc")
        buf.write("\u0dd1\u0dd6\u0dd8\u0dd8\u0dda\u0de1\u0df4\u0df5\u0e33")
        buf.write("\u0e33\u0e36\u0e3c\u0e49\u0e50\u0e52\u0e5b\u0eb3\u0eb3")
        buf.write("\u0eb6\u0ebb\u0ebd\u0ebe\u0eca\u0ecf\u0ed2\u0edb\u0f1a")
        buf.write("\u0f1b\u0f22\u0f2b\u0f37\u0f37\u0f39\u0f39\u0f3b\u0f3b")
        buf.write("\u0f40\u0f41\u0f73\u0f86\u0f88\u0f89\u0f92\u0f99\u0f9b")
        buf.write("\u0fbe\u0fc8\u0fc8\u102e\u1034\u1038\u103b\u1042\u104b")
        buf.write("\u1058\u105b\u1361\u1361\u136b\u1373\u1714\u1716\u1734")
        buf.write("\u1736\u1754\u1755\u1774\u1775\u17b8\u17d5\u17df\u17df")
        buf.write("\u17e2\u17eb\u180d\u180f\u1812\u181b\u18ab\u18ab\u1922")
        buf.write("\u192d\u1932\u193d\u1948\u1951\u19b2\u19c2\u19ca\u19cb")
        buf.write("\u19d2\u19db\u1a19\u1a1d\u1dc2\u1dc5\u2041\u2042\u2056")
        buf.write("\u2056\u20d2\u20de\u20e3\u20e3\u20e7\u20ed\u302c\u3031")
        buf.write("\u309b\u309c\ua804\ua804\ua808\ua808\ua80d\ua80d\ua825")
        buf.write("\ua829\ufb20\ufb20\ufe02\ufe11\ufe22\ufe25\ufe35\ufe36")
        buf.write("\ufe4f\ufe51\uff12\uff1b\uff41\uff41\2\u03e4\2\3\3\2\2")
        buf.write("\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2")
        buf.write("\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write("\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3")
        buf.write("\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2")
        buf.write("\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2")
        buf.write("\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\2")
        buf.write("9\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2")
        buf.write("\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2")
        buf.write("\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2")
        buf.write("\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3")
        buf.write("\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i")
        buf.write("\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2\2\2")
        buf.write("s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2")
        buf.write("\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083\3\2")
        buf.write("\2\2\2\u0085\3\2\2\2\2\u0087\3\2\2\2\2\u0089\3\2\2\2\2")
        buf.write("\u008b\3\2\2\2\2\u008d\3\2\2\2\2\u008f\3\2\2\2\2\u0091")
        buf.write("\3\2\2\2\2\u0093\3\2\2\2\2\u0095\3\2\2\2\2\u0097\3\2\2")
        buf.write("\2\2\u0099\3\2\2\2\2\u009b\3\2\2\2\2\u009d\3\2\2\2\2\u009f")
        buf.write("\3\2\2\2\2\u00a1\3\2\2\2\2\u00a3\3\2\2\2\2\u00a5\3\2\2")
        buf.write("\2\2\u00a7\3\2\2\2\2\u00a9\3\2\2\2\2\u00ab\3\2\2\2\2\u00ad")
        buf.write("\3\2\2\2\2\u00af\3\2\2\2\2\u00b1\3\2\2\2\2\u00b3\3\2\2")
        buf.write("\2\2\u00b5\3\2\2\2\2\u00b7\3\2\2\2\2\u00b9\3\2\2\2\2\u00bb")
        buf.write("\3\2\2\2\2\u00bd\3\2\2\2\2\u00bf\3\2\2\2\2\u00c1\3\2\2")
        buf.write("\2\2\u00c3\3\2\2\2\2\u00c5\3\2\2\2\2\u00c7\3\2\2\2\2\u00c9")
        buf.write("\3\2\2\2\2\u00cb\3\2\2\2\2\u00cd\3\2\2\2\2\u00cf\3\2\2")
        buf.write("\2\2\u00d1\3\2\2\2\2\u00d3\3\2\2\2\2\u00d5\3\2\2\2\2\u00d7")
        buf.write("\3\2\2\2\3\u010f\3\2\2\2\5\u0117\3\2\2\2\7\u0120\3\2\2")
        buf.write("\2\t\u0128\3\2\2\2\13\u0131\3\2\2\2\r\u013b\3\2\2\2\17")
        buf.write("\u0144\3\2\2\2\21\u014b\3\2\2\2\23\u014f\3\2\2\2\25\u0157")
        buf.write("\3\2\2\2\27\u015b\3\2\2\2\31\u015f\3\2\2\2\33\u0164\3")
        buf.write("\2\2\2\35\u0170\3\2\2\2\37\u0173\3\2\2\2!\u0177\3\2\2")
        buf.write("\2#\u017b\3\2\2\2%\u0182\3\2\2\2\'\u0188\3\2\2\2)\u018d")
        buf.write("\3\2\2\2+\u0194\3\2\2\2-\u0197\3\2\2\2/\u019e\3\2\2\2")
        buf.write("\61\u01a7\3\2\2\2\63\u01ae\3\2\2\2\65\u01b1\3\2\2\2\67")
        buf.write("\u01b6\3\2\2\29\u01bb\3\2\2\2;\u01c1\3\2\2\2=\u01c5\3")
        buf.write("\2\2\2?\u01c8\3\2\2\2A\u01cc\3\2\2\2C\u01d4\3\2\2\2E\u01d9")
        buf.write("\3\2\2\2G\u01e0\3\2\2\2I\u01e7\3\2\2\2K\u01ea\3\2\2\2")
        buf.write("M\u01ee\3\2\2\2O\u01f2\3\2\2\2Q\u01f5\3\2\2\2S\u01fa\3")
        buf.write("\2\2\2U\u01ff\3\2\2\2W\u0205\3\2\2\2Y\u020b\3\2\2\2[\u0211")
        buf.write("\3\2\2\2]\u0215\3\2\2\2_\u021a\3\2\2\2a\u0223\3\2\2\2")
        buf.write("c\u0235\3\2\2\2e\u0239\3\2\2\2g\u0241\3\2\2\2i\u024a\3")
        buf.write("\2\2\2k\u025e\3\2\2\2m\u0260\3\2\2\2o\u0267\3\2\2\2q\u026e")
        buf.write("\3\2\2\2s\u0277\3\2\2\2u\u027b\3\2\2\2w\u027f\3\2\2\2")
        buf.write("y\u0281\3\2\2\2{\u0285\3\2\2\2}\u0287\3\2\2\2\177\u028a")
        buf.write("\3\2\2\2\u0081\u028d\3\2\2\2\u0083\u028f\3\2\2\2\u0085")
        buf.write("\u0291\3\2\2\2\u0087\u0293\3\2\2\2\u0089\u0296\3\2\2\2")
        buf.write("\u008b\u0298\3\2\2\2\u008d\u029b\3\2\2\2\u008f\u029e\3")
        buf.write("\2\2\2\u0091\u02a0\3\2\2\2\u0093\u02a2\3\2\2\2\u0095\u02a4")
        buf.write("\3\2\2\2\u0097\u02a7\3\2\2\2\u0099\u02aa\3\2\2\2\u009b")
        buf.write("\u02ac\3\2\2\2\u009d\u02ae\3\2\2\2\u009f\u02b0\3\2\2\2")
        buf.write("\u00a1\u02b2\3\2\2\2\u00a3\u02b5\3\2\2\2\u00a5\u02b7\3")
        buf.write("\2\2\2\u00a7\u02ba\3\2\2\2\u00a9\u02bd\3\2\2\2\u00ab\u02bf")
        buf.write("\3\2\2\2\u00ad\u02c1\3\2\2\2\u00af\u02c4\3\2\2\2\u00b1")
        buf.write("\u02c7\3\2\2\2\u00b3\u02ca\3\2\2\2\u00b5\u02cd\3\2\2\2")
        buf.write("\u00b7\u02d0\3\2\2\2\u00b9\u02d2\3\2\2\2\u00bb\u02d5\3")
        buf.write("\2\2\2\u00bd\u02d8\3\2\2\2\u00bf\u02db\3\2\2\2\u00c1\u02de")
        buf.write("\3\2\2\2\u00c3\u02e1\3\2\2\2\u00c5\u02e4\3\2\2\2\u00c7")
        buf.write("\u02e7\3\2\2\2\u00c9\u02ea\3\2\2\2\u00cb\u02ed\3\2\2\2")
        buf.write("\u00cd\u02f0\3\2\2\2\u00cf\u02f4\3\2\2\2\u00d1\u02f8\3")
        buf.write("\2\2\2\u00d3\u02fc\3\2\2\2\u00d5\u0303\3\2\2\2\u00d7\u0307")
        buf.write("\3\2\2\2\u00d9\u031b\3\2\2\2\u00db\u0337\3\2\2\2\u00dd")
        buf.write("\u033b\3\2\2\2\u00df\u033d\3\2\2\2\u00e1\u033f\3\2\2\2")
        buf.write("\u00e3\u0342\3\2\2\2\u00e5\u0344\3\2\2\2\u00e7\u0346\3")
        buf.write("\2\2\2\u00e9\u0348\3\2\2\2\u00eb\u034a\3\2\2\2\u00ed\u0353")
        buf.write("\3\2\2\2\u00ef\u0357\3\2\2\2\u00f1\u035c\3\2\2\2\u00f3")
        buf.write("\u0360\3\2\2\2\u00f5\u0366\3\2\2\2\u00f7\u0381\3\2\2\2")
        buf.write("\u00f9\u039d\3\2\2\2\u00fb\u03a1\3\2\2\2\u00fd\u03a4\3")
        buf.write("\2\2\2\u00ff\u03a7\3\2\2\2\u0101\u03aa\3\2\2\2\u0103\u03ac")
        buf.write("\3\2\2\2\u0105\u03b0\3\2\2\2\u0107\u03b4\3\2\2\2\u0109")
        buf.write("\u03bb\3\2\2\2\u010b\u03c7\3\2\2\2\u010d\u03cb\3\2\2\2")
        buf.write("\u010f\u0110\7r\2\2\u0110\u0111\7c\2\2\u0111\u0112\7e")
        buf.write("\2\2\u0112\u0113\7m\2\2\u0113\u0114\7c\2\2\u0114\u0115")
        buf.write("\7i\2\2\u0115\u0116\7g\2\2\u0116\4\3\2\2\2\u0117\u0118")
        buf.write("\7c\2\2\u0118\u0119\7d\2\2\u0119\u011a\7u\2\2\u011a\u011b")
        buf.write("\7v\2\2\u011b\u011c\7t\2\2\u011c\u011d\7c\2\2\u011d\u011e")
        buf.write("\7e\2\2\u011e\u011f\7v\2\2\u011f\6\3\2\2\2\u0120\u0121")
        buf.write("\7g\2\2\u0121\u0122\7z\2\2\u0122\u0123\7v\2\2\u0123\u0124")
        buf.write("\7g\2\2\u0124\u0125\7p\2\2\u0125\u0126\7f\2\2\u0126\u0127")
        buf.write("\7u\2\2\u0127\b\3\2\2\2\u0128\u0129\7e\2\2\u0129\u012a")
        buf.write("\7q\2\2\u012a\u012b\7p\2\2\u012b\u012c\7v\2\2\u012c\u012d")
        buf.write("\7c\2\2\u012d\u012e\7k\2\2\u012e\u012f\7p\2\2\u012f\u0130")
        buf.write("\7u\2\2\u0130\n\3\2\2\2\u0131\u0132\7e\2\2\u0132\u0133")
        buf.write("\7q\2\2\u0133\u0134\7p\2\2\u0134\u0135\7v\2\2\u0135\u0136")
        buf.write("\7c\2\2\u0136\u0137\7k\2\2\u0137\u0138\7p\2\2\u0138\u0139")
        buf.write("\7g\2\2\u0139\u013a\7t\2\2\u013a\f\3\2\2\2\u013b\u013c")
        buf.write("\7q\2\2\u013c\u013d\7r\2\2\u013d\u013e\7r\2\2\u013e\u013f")
        buf.write("\7q\2\2\u013f\u0140\7u\2\2\u0140\u0141\7k\2\2\u0141\u0142")
        buf.write("\7v\2\2\u0142\u0143\7g\2\2\u0143\16\3\2\2\2\u0144\u0145")
        buf.write("\7t\2\2\u0145\u0146\7g\2\2\u0146\u0147\7h\2\2\u0147\u0148")
        buf.write("\7g\2\2\u0148\u0149\7t\2\2\u0149\u014a\7u\2\2\u014a\20")
        buf.write("\3\2\2\2\u014b\u014c\7g\2\2\u014c\u014d\7p\2\2\u014d\u014e")
        buf.write("\7f\2\2\u014e\22\3\2\2\2\u014f\u0150\7f\2\2\u0150\u0151")
        buf.write("\7g\2\2\u0151\u0152\7t\2\2\u0152\u0153\7k\2\2\u0153\u0154")
        buf.write("\7x\2\2\u0154\u0155\7g\2\2\u0155\u0156\7f\2\2\u0156\24")
        buf.write("\3\2\2\2\u0157\u0158\7i\2\2\u0158\u0159\7g\2\2\u0159\u015a")
        buf.write("\7v\2\2\u015a\26\3\2\2\2\u015b\u015c\7u\2\2\u015c\u015d")
        buf.write("\7g\2\2\u015d\u015e\7v\2\2\u015e\30\3\2\2\2\u015f\u0160")
        buf.write("\7g\2\2\u0160\u0161\7p\2\2\u0161\u0162\7w\2\2\u0162\u0163")
        buf.write("\7o\2\2\u0163\32\3\2\2\2\u0164\u0165\7c\2\2\u0165\u0166")
        buf.write("\7u\2\2\u0166\u0167\7u\2\2\u0167\u0168\7q\2\2\u0168\u0169")
        buf.write("\7e\2\2\u0169\u016a\7k\2\2\u016a\u016b\7c\2\2\u016b\u016c")
        buf.write("\7v\2\2\u016c\u016d\7k\2\2\u016d\u016e\7q\2\2\u016e\u016f")
        buf.write("\7p\2\2\u016f\34\3\2\2\2\u0170\u0171\7\60\2\2\u0171\u0172")
        buf.write("\7\60\2\2\u0172\36\3\2\2\2\u0173\u0174\5\u00f1y\2\u0174")
        buf.write("\u0175\7\60\2\2\u0175\u0176\7\60\2\2\u0176 \3\2\2\2\u0177")
        buf.write("\u0178\7f\2\2\u0178\u0179\7g\2\2\u0179\u017a\7h\2\2\u017a")
        buf.write("\"\3\2\2\2\u017b\u017c\7t\2\2\u017c\u017d\7g\2\2\u017d")
        buf.write("\u017e\7v\2\2\u017e\u017f\7w\2\2\u017f\u0180\7t\2\2\u0180")
        buf.write("\u0181\7p\2\2\u0181$\3\2\2\2\u0182\u0183\7t\2\2\u0183")
        buf.write("\u0184\7c\2\2\u0184\u0185\7k\2\2\u0185\u0186\7u\2\2\u0186")
        buf.write("\u0187\7g\2\2\u0187&\3\2\2\2\u0188\u0189\7h\2\2\u0189")
        buf.write("\u018a\7t\2\2\u018a\u018b\7q\2\2\u018b\u018c\7o\2\2\u018c")
        buf.write("(\3\2\2\2\u018d\u018e\7k\2\2\u018e\u018f\7o\2\2\u018f")
        buf.write("\u0190\7r\2\2\u0190\u0191\7q\2\2\u0191\u0192\7t\2\2\u0192")
        buf.write("\u0193\7v\2\2\u0193*\3\2\2\2\u0194\u0195\7c\2\2\u0195")
        buf.write("\u0196\7u\2\2\u0196,\3\2\2\2\u0197\u0198\7i\2\2\u0198")
        buf.write("\u0199\7n\2\2\u0199\u019a\7q\2\2\u019a\u019b\7d\2\2\u019b")
        buf.write("\u019c\7c\2\2\u019c\u019d\7n\2\2\u019d.\3\2\2\2\u019e")
        buf.write("\u019f\7p\2\2\u019f\u01a0\7q\2\2\u01a0\u01a1\7p\2\2\u01a1")
        buf.write("\u01a2\7n\2\2\u01a2\u01a3\7q\2\2\u01a3\u01a4\7e\2\2\u01a4")
        buf.write("\u01a5\7c\2\2\u01a5\u01a6\7n\2\2\u01a6\60\3\2\2\2\u01a7")
        buf.write("\u01a8\7c\2\2\u01a8\u01a9\7u\2\2\u01a9\u01aa\7u\2\2\u01aa")
        buf.write("\u01ab\7g\2\2\u01ab\u01ac\7t\2\2\u01ac\u01ad\7v\2\2\u01ad")
        buf.write("\62\3\2\2\2\u01ae\u01af\7k\2\2\u01af\u01b0\7h\2\2\u01b0")
        buf.write("\64\3\2\2\2\u01b1\u01b2\7g\2\2\u01b2\u01b3\7n\2\2\u01b3")
        buf.write("\u01b4\7k\2\2\u01b4\u01b5\7h\2\2\u01b5\66\3\2\2\2\u01b6")
        buf.write("\u01b7\7g\2\2\u01b7\u01b8\7n\2\2\u01b8\u01b9\7u\2\2\u01b9")
        buf.write("\u01ba\7g\2\2\u01ba8\3\2\2\2\u01bb\u01bc\7y\2\2\u01bc")
        buf.write("\u01bd\7j\2\2\u01bd\u01be\7k\2\2\u01be\u01bf\7n\2\2\u01bf")
        buf.write("\u01c0\7g\2\2\u01c0:\3\2\2\2\u01c1\u01c2\7h\2\2\u01c2")
        buf.write("\u01c3\7q\2\2\u01c3\u01c4\7t\2\2\u01c4<\3\2\2\2\u01c5")
        buf.write("\u01c6\7k\2\2\u01c6\u01c7\7p\2\2\u01c7>\3\2\2\2\u01c8")
        buf.write("\u01c9\7v\2\2\u01c9\u01ca\7t\2\2\u01ca\u01cb\7{\2\2\u01cb")
        buf.write("@\3\2\2\2\u01cc\u01cd\7h\2\2\u01cd\u01ce\7k\2\2\u01ce")
        buf.write("\u01cf\7p\2\2\u01cf\u01d0\7c\2\2\u01d0\u01d1\7n\2\2\u01d1")
        buf.write("\u01d2\7n\2\2\u01d2\u01d3\7{\2\2\u01d3B\3\2\2\2\u01d4")
        buf.write("\u01d5\7y\2\2\u01d5\u01d6\7k\2\2\u01d6\u01d7\7v\2\2\u01d7")
        buf.write("\u01d8\7j\2\2\u01d8D\3\2\2\2\u01d9\u01da\7g\2\2\u01da")
        buf.write("\u01db\7z\2\2\u01db\u01dc\7e\2\2\u01dc\u01dd\7g\2\2\u01dd")
        buf.write("\u01de\7r\2\2\u01de\u01df\7v\2\2\u01dfF\3\2\2\2\u01e0")
        buf.write("\u01e1\7n\2\2\u01e1\u01e2\7c\2\2\u01e2\u01e3\7o\2\2\u01e3")
        buf.write("\u01e4\7d\2\2\u01e4\u01e5\7f\2\2\u01e5\u01e6\7c\2\2\u01e6")
        buf.write("H\3\2\2\2\u01e7\u01e8\7q\2\2\u01e8\u01e9\7t\2\2\u01e9")
        buf.write("J\3\2\2\2\u01ea\u01eb\7c\2\2\u01eb\u01ec\7p\2\2\u01ec")
        buf.write("\u01ed\7f\2\2\u01edL\3\2\2\2\u01ee\u01ef\7p\2\2\u01ef")
        buf.write("\u01f0\7q\2\2\u01f0\u01f1\7v\2\2\u01f1N\3\2\2\2\u01f2")
        buf.write("\u01f3\7k\2\2\u01f3\u01f4\7u\2\2\u01f4P\3\2\2\2\u01f5")
        buf.write("\u01f6\7P\2\2\u01f6\u01f7\7q\2\2\u01f7\u01f8\7p\2\2\u01f8")
        buf.write("\u01f9\7g\2\2\u01f9R\3\2\2\2\u01fa\u01fb\7V\2\2\u01fb")
        buf.write("\u01fc\7t\2\2\u01fc\u01fd\7w\2\2\u01fd\u01fe\7g\2\2\u01fe")
        buf.write("T\3\2\2\2\u01ff\u0200\7H\2\2\u0200\u0201\7c\2\2\u0201")
        buf.write("\u0202\7n\2\2\u0202\u0203\7u\2\2\u0203\u0204\7g\2\2\u0204")
        buf.write("V\3\2\2\2\u0205\u0206\7e\2\2\u0206\u0207\7n\2\2\u0207")
        buf.write("\u0208\7c\2\2\u0208\u0209\7u\2\2\u0209\u020a\7u\2\2\u020a")
        buf.write("X\3\2\2\2\u020b\u020c\7{\2\2\u020c\u020d\7k\2\2\u020d")
        buf.write("\u020e\7g\2\2\u020e\u020f\7n\2\2\u020f\u0210\7f\2\2\u0210")
        buf.write("Z\3\2\2\2\u0211\u0212\7f\2\2\u0212\u0213\7g\2\2\u0213")
        buf.write("\u0214\7n\2\2\u0214\\\3\2\2\2\u0215\u0216\7r\2\2\u0216")
        buf.write("\u0217\7c\2\2\u0217\u0218\7u\2\2\u0218\u0219\7u\2\2\u0219")
        buf.write("^\3\2\2\2\u021a\u021b\7e\2\2\u021b\u021c\7q\2\2\u021c")
        buf.write("\u021d\7p\2\2\u021d\u021e\7v\2\2\u021e\u021f\7k\2\2\u021f")
        buf.write("\u0220\7p\2\2\u0220\u0221\7w\2\2\u0221\u0222\7g\2\2\u0222")
        buf.write("`\3\2\2\2\u0223\u0224\7d\2\2\u0224\u0225\7t\2\2\u0225")
        buf.write("\u0226\7g\2\2\u0226\u0227\7c\2\2\u0227\u0228\7m\2\2\u0228")
        buf.write("b\3\2\2\2\u0229\u022a\6\62\2\2\u022a\u0236\5\u0105\u0083")
        buf.write("\2\u022b\u022d\7\17\2\2\u022c\u022b\3\2\2\2\u022c\u022d")
        buf.write("\3\2\2\2\u022d\u022e\3\2\2\2\u022e\u0231\7\f\2\2\u022f")
        buf.write("\u0231\7\17\2\2\u0230\u022c\3\2\2\2\u0230\u022f\3\2\2")
        buf.write("\2\u0231\u0233\3\2\2\2\u0232\u0234\5\u0105\u0083\2\u0233")
        buf.write("\u0232\3\2\2\2\u0233\u0234\3\2\2\2\u0234\u0236\3\2\2\2")
        buf.write("\u0235\u0229\3\2\2\2\u0235\u0230\3\2\2\2\u0236\u0237\3")
        buf.write("\2\2\2\u0237\u0238\b\62\2\2\u0238d\3\2\2\2\u0239\u023d")
        buf.write("\5\u010b\u0086\2\u023a\u023c\5\u010d\u0087\2\u023b\u023a")
        buf.write("\3\2\2\2\u023c\u023f\3\2\2\2\u023d\u023b\3\2\2\2\u023d")
        buf.write("\u023e\3\2\2\2\u023ef\3\2\2\2\u023f\u023d\3\2\2\2\u0240")
        buf.write("\u0242\t\2\2\2\u0241\u0240\3\2\2\2\u0241\u0242\3\2\2\2")
        buf.write("\u0242\u0244\3\2\2\2\u0243\u0245\t\3\2\2\u0244\u0243\3")
        buf.write("\2\2\2\u0244\u0245\3\2\2\2\u0245\u0248\3\2\2\2\u0246\u0249")
        buf.write("\5\u00d9m\2\u0247\u0249\5\u00dbn\2\u0248\u0246\3\2\2\2")
        buf.write("\u0248\u0247\3\2\2\2\u0249h\3\2\2\2\u024a\u024c\t\4\2")
        buf.write("\2\u024b\u024d\t\3\2\2\u024c\u024b\3\2\2\2\u024c\u024d")
        buf.write("\3\2\2\2\u024d\u0250\3\2\2\2\u024e\u0251\5\u00f7|\2\u024f")
        buf.write("\u0251\5\u00f9}\2\u0250\u024e\3\2\2\2\u0250\u024f\3\2")
        buf.write("\2\2\u0251j\3\2\2\2\u0252\u0256\5\u00e3r\2\u0253\u0255")
        buf.write("\5\u00e5s\2\u0254\u0253\3\2\2\2\u0255\u0258\3\2\2\2\u0256")
        buf.write("\u0254\3\2\2\2\u0256\u0257\3\2\2\2\u0257\u025f\3\2\2\2")
        buf.write("\u0258\u0256\3\2\2\2\u0259\u025b\7\62\2\2\u025a\u0259")
        buf.write("\3\2\2\2\u025b\u025c\3\2\2\2\u025c\u025a\3\2\2\2\u025c")
        buf.write("\u025d\3\2\2\2\u025d\u025f\3\2\2\2\u025e\u0252\3\2\2\2")
        buf.write("\u025e\u025a\3\2\2\2\u025fl\3\2\2\2\u0260\u0261\7\62\2")
        buf.write("\2\u0261\u0263\t\5\2\2\u0262\u0264\5\u00e7t\2\u0263\u0262")
        buf.write("\3\2\2\2\u0264\u0265\3\2\2\2\u0265\u0263\3\2\2\2\u0265")
        buf.write("\u0266\3\2\2\2\u0266n\3\2\2\2\u0267\u0268\7\62\2\2\u0268")
        buf.write("\u026a\t\6\2\2\u0269\u026b\5\u00e9u\2\u026a\u0269\3\2")
        buf.write("\2\2\u026b\u026c\3\2\2\2\u026c\u026a\3\2\2\2\u026c\u026d")
        buf.write("\3\2\2\2\u026dp\3\2\2\2\u026e\u026f\7\62\2\2\u026f\u0271")
        buf.write("\t\4\2\2\u0270\u0272\5\u00ebv\2\u0271\u0270\3\2\2\2\u0272")
        buf.write("\u0273\3\2\2\2\u0273\u0271\3\2\2\2\u0273\u0274\3\2\2\2")
        buf.write("\u0274r\3\2\2\2\u0275\u0278\5\u00edw\2\u0276\u0278\5\u00ef")
        buf.write("x\2\u0277\u0275\3\2\2\2\u0277\u0276\3\2\2\2\u0278t\3\2")
        buf.write("\2\2\u0279\u027c\5s:\2\u027a\u027c\5\u00f1y\2\u027b\u0279")
        buf.write("\3\2\2\2\u027b\u027a\3\2\2\2\u027c\u027d\3\2\2\2\u027d")
        buf.write("\u027e\t\7\2\2\u027ev\3\2\2\2\u027f\u0280\7\60\2\2\u0280")
        buf.write("x\3\2\2\2\u0281\u0282\7\60\2\2\u0282\u0283\7\60\2\2\u0283")
        buf.write("\u0284\7\60\2\2\u0284z\3\2\2\2\u0285\u0286\7,\2\2\u0286")
        buf.write("|\3\2\2\2\u0287\u0288\7*\2\2\u0288\u0289\b?\3\2\u0289")
        buf.write("~\3\2\2\2\u028a\u028b\7+\2\2\u028b\u028c\b@\4\2\u028c")
        buf.write("\u0080\3\2\2\2\u028d\u028e\7.\2\2\u028e\u0082\3\2\2\2")
        buf.write("\u028f\u0290\7<\2\2\u0290\u0084\3\2\2\2\u0291\u0292\7")
        buf.write("=\2\2\u0292\u0086\3\2\2\2\u0293\u0294\7,\2\2\u0294\u0295")
        buf.write("\7,\2\2\u0295\u0088\3\2\2\2\u0296\u0297\7?\2\2\u0297\u008a")
        buf.write("\3\2\2\2\u0298\u0299\7]\2\2\u0299\u029a\bF\5\2\u029a\u008c")
        buf.write("\3\2\2\2\u029b\u029c\7_\2\2\u029c\u029d\bG\6\2\u029d\u008e")
        buf.write("\3\2\2\2\u029e\u029f\7~\2\2\u029f\u0090\3\2\2\2\u02a0")
        buf.write("\u02a1\7`\2\2\u02a1\u0092\3\2\2\2\u02a2\u02a3\7(\2\2\u02a3")
        buf.write("\u0094\3\2\2\2\u02a4\u02a5\7>\2\2\u02a5\u02a6\7>\2\2\u02a6")
        buf.write("\u0096\3\2\2\2\u02a7\u02a8\7@\2\2\u02a8\u02a9\7@\2\2\u02a9")
        buf.write("\u0098\3\2\2\2\u02aa\u02ab\7-\2\2\u02ab\u009a\3\2\2\2")
        buf.write("\u02ac\u02ad\7/\2\2\u02ad\u009c\3\2\2\2\u02ae\u02af\7")
        buf.write("\61\2\2\u02af\u009e\3\2\2\2\u02b0\u02b1\7\'\2\2\u02b1")
        buf.write("\u00a0\3\2\2\2\u02b2\u02b3\7\61\2\2\u02b3\u02b4\7\61\2")
        buf.write("\2\u02b4\u00a2\3\2\2\2\u02b5\u02b6\7\u0080\2\2\u02b6\u00a4")
        buf.write("\3\2\2\2\u02b7\u02b8\7}\2\2\u02b8\u02b9\bS\7\2\u02b9\u00a6")
        buf.write("\3\2\2\2\u02ba\u02bb\7\177\2\2\u02bb\u02bc\bT\b\2\u02bc")
        buf.write("\u00a8\3\2\2\2\u02bd\u02be\7>\2\2\u02be\u00aa\3\2\2\2")
        buf.write("\u02bf\u02c0\7@\2\2\u02c0\u00ac\3\2\2\2\u02c1\u02c2\7")
        buf.write("?\2\2\u02c2\u02c3\7?\2\2\u02c3\u00ae\3\2\2\2\u02c4\u02c5")
        buf.write("\7@\2\2\u02c5\u02c6\7?\2\2\u02c6\u00b0\3\2\2\2\u02c7\u02c8")
        buf.write("\7>\2\2\u02c8\u02c9\7?\2\2\u02c9\u00b2\3\2\2\2\u02ca\u02cb")
        buf.write("\7>\2\2\u02cb\u02cc\7@\2\2\u02cc\u00b4\3\2\2\2\u02cd\u02ce")
        buf.write("\7#\2\2\u02ce\u02cf\7?\2\2\u02cf\u00b6\3\2\2\2\u02d0\u02d1")
        buf.write("\7B\2\2\u02d1\u00b8\3\2\2\2\u02d2\u02d3\7/\2\2\u02d3\u02d4")
        buf.write("\7@\2\2\u02d4\u00ba\3\2\2\2\u02d5\u02d6\7-\2\2\u02d6\u02d7")
        buf.write("\7?\2\2\u02d7\u00bc\3\2\2\2\u02d8\u02d9\7/\2\2\u02d9\u02da")
        buf.write("\7?\2\2\u02da\u00be\3\2\2\2\u02db\u02dc\7,\2\2\u02dc\u02dd")
        buf.write("\7?\2\2\u02dd\u00c0\3\2\2\2\u02de\u02df\7B\2\2\u02df\u02e0")
        buf.write("\7?\2\2\u02e0\u00c2\3\2\2\2\u02e1\u02e2\7\61\2\2\u02e2")
        buf.write("\u02e3\7?\2\2\u02e3\u00c4\3\2\2\2\u02e4\u02e5\7\'\2\2")
        buf.write("\u02e5\u02e6\7?\2\2\u02e6\u00c6\3\2\2\2\u02e7\u02e8\7")
        buf.write("(\2\2\u02e8\u02e9\7?\2\2\u02e9\u00c8\3\2\2\2\u02ea\u02eb")
        buf.write("\7~\2\2\u02eb\u02ec\7?\2\2\u02ec\u00ca\3\2\2\2\u02ed\u02ee")
        buf.write("\7`\2\2\u02ee\u02ef\7?\2\2\u02ef\u00cc\3\2\2\2\u02f0\u02f1")
        buf.write("\7>\2\2\u02f1\u02f2\7>\2\2\u02f2\u02f3\7?\2\2\u02f3\u00ce")
        buf.write("\3\2\2\2\u02f4\u02f5\7@\2\2\u02f5\u02f6\7@\2\2\u02f6\u02f7")
        buf.write("\7?\2\2\u02f7\u00d0\3\2\2\2\u02f8\u02f9\7,\2\2\u02f9\u02fa")
        buf.write("\7,\2\2\u02fa\u02fb\7?\2\2\u02fb\u00d2\3\2\2\2\u02fc\u02fd")
        buf.write("\7\61\2\2\u02fd\u02fe\7\61\2\2\u02fe\u02ff\7?\2\2\u02ff")
        buf.write("\u00d4\3\2\2\2\u0300\u0304\5\u0105\u0083\2\u0301\u0304")
        buf.write("\5\u0107\u0084\2\u0302\u0304\5\u0109\u0085\2\u0303\u0300")
        buf.write("\3\2\2\2\u0303\u0301\3\2\2\2\u0303\u0302\3\2\2\2\u0304")
        buf.write("\u0305\3\2\2\2\u0305\u0306\bk\t\2\u0306\u00d6\3\2\2\2")
        buf.write("\u0307\u0308\13\2\2\2\u0308\u00d8\3\2\2\2\u0309\u030e")
        buf.write("\7)\2\2\u030a\u030d\5\u00e1q\2\u030b\u030d\n\b\2\2\u030c")
        buf.write("\u030a\3\2\2\2\u030c\u030b\3\2\2\2\u030d\u0310\3\2\2\2")
        buf.write("\u030e\u030c\3\2\2\2\u030e\u030f\3\2\2\2\u030f\u0311\3")
        buf.write("\2\2\2\u0310\u030e\3\2\2\2\u0311\u031c\7)\2\2\u0312\u0317")
        buf.write("\7$\2\2\u0313\u0316\5\u00e1q\2\u0314\u0316\n\t\2\2\u0315")
        buf.write("\u0313\3\2\2\2\u0315\u0314\3\2\2\2\u0316\u0319\3\2\2\2")
        buf.write("\u0317\u0315\3\2\2\2\u0317\u0318\3\2\2\2\u0318\u031a\3")
        buf.write("\2\2\2\u0319\u0317\3\2\2\2\u031a\u031c\7$\2\2\u031b\u0309")
        buf.write("\3\2\2\2\u031b\u0312\3\2\2\2\u031c\u00da\3\2\2\2\u031d")
        buf.write("\u031e\7)\2\2\u031e\u031f\7)\2\2\u031f\u0320\7)\2\2\u0320")
        buf.write("\u0324\3\2\2\2\u0321\u0323\5\u00ddo\2\u0322\u0321\3\2")
        buf.write("\2\2\u0323\u0326\3\2\2\2\u0324\u0325\3\2\2\2\u0324\u0322")
        buf.write("\3\2\2\2\u0325\u0327\3\2\2\2\u0326\u0324\3\2\2\2\u0327")
        buf.write("\u0328\7)\2\2\u0328\u0329\7)\2\2\u0329\u0338\7)\2\2\u032a")
        buf.write("\u032b\7$\2\2\u032b\u032c\7$\2\2\u032c\u032d\7$\2\2\u032d")
        buf.write("\u0331\3\2\2\2\u032e\u0330\5\u00ddo\2\u032f\u032e\3\2")
        buf.write("\2\2\u0330\u0333\3\2\2\2\u0331\u0332\3\2\2\2\u0331\u032f")
        buf.write("\3\2\2\2\u0332\u0334\3\2\2\2\u0333\u0331\3\2\2\2\u0334")
        buf.write("\u0335\7$\2\2\u0335\u0336\7$\2\2\u0336\u0338\7$\2\2\u0337")
        buf.write("\u031d\3\2\2\2\u0337\u032a\3\2\2\2\u0338\u00dc\3\2\2\2")
        buf.write("\u0339\u033c\5\u00dfp\2\u033a\u033c\5\u00e1q\2\u033b\u0339")
        buf.write("\3\2\2\2\u033b\u033a\3\2\2\2\u033c\u00de\3\2\2\2\u033d")
        buf.write("\u033e\n\n\2\2\u033e\u00e0\3\2\2\2\u033f\u0340\7^\2\2")
        buf.write("\u0340\u0341\13\2\2\2\u0341\u00e2\3\2\2\2\u0342\u0343")
        buf.write("\t\13\2\2\u0343\u00e4\3\2\2\2\u0344\u0345\t\f\2\2\u0345")
        buf.write("\u00e6\3\2\2\2\u0346\u0347\t\r\2\2\u0347\u00e8\3\2\2\2")
        buf.write("\u0348\u0349\t\16\2\2\u0349\u00ea\3\2\2\2\u034a\u034b")
        buf.write("\t\17\2\2\u034b\u00ec\3\2\2\2\u034c\u034e\5\u00f1y\2\u034d")
        buf.write("\u034c\3\2\2\2\u034d\u034e\3\2\2\2\u034e\u034f\3\2\2\2")
        buf.write("\u034f\u0354\5\u00f3z\2\u0350\u0351\5\u00f1y\2\u0351\u0352")
        buf.write("\7\60\2\2\u0352\u0354\3\2\2\2\u0353\u034d\3\2\2\2\u0353")
        buf.write("\u0350\3\2\2\2\u0354\u00ee\3\2\2\2\u0355\u0358\5\u00f1")
        buf.write("y\2\u0356\u0358\5\u00edw\2\u0357\u0355\3\2\2\2\u0357\u0356")
        buf.write("\3\2\2\2\u0358\u0359\3\2\2\2\u0359\u035a\5\u00f5{\2\u035a")
        buf.write("\u00f0\3\2\2\2\u035b\u035d\5\u00e5s\2\u035c\u035b\3\2")
        buf.write("\2\2\u035d\u035e\3\2\2\2\u035e\u035c\3\2\2\2\u035e\u035f")
        buf.write("\3\2\2\2\u035f\u00f2\3\2\2\2\u0360\u0362\7\60\2\2\u0361")
        buf.write("\u0363\5\u00e5s\2\u0362\u0361\3\2\2\2\u0363\u0364\3\2")
        buf.write("\2\2\u0364\u0362\3\2\2\2\u0364\u0365\3\2\2\2\u0365\u00f4")
        buf.write("\3\2\2\2\u0366\u0368\t\20\2\2\u0367\u0369\t\21\2\2\u0368")
        buf.write("\u0367\3\2\2\2\u0368\u0369\3\2\2\2\u0369\u036b\3\2\2\2")
        buf.write("\u036a\u036c\5\u00e5s\2\u036b\u036a\3\2\2\2\u036c\u036d")
        buf.write("\3\2\2\2\u036d\u036b\3\2\2\2\u036d\u036e\3\2\2\2\u036e")
        buf.write("\u00f6\3\2\2\2\u036f\u0374\7)\2\2\u0370\u0373\5\u00fd")
        buf.write("\177\2\u0371\u0373\5\u0103\u0082\2\u0372\u0370\3\2\2\2")
        buf.write("\u0372\u0371\3\2\2\2\u0373\u0376\3\2\2\2\u0374\u0372\3")
        buf.write("\2\2\2\u0374\u0375\3\2\2\2\u0375\u0377\3\2\2\2\u0376\u0374")
        buf.write("\3\2\2\2\u0377\u0382\7)\2\2\u0378\u037d\7$\2\2\u0379\u037c")
        buf.write("\5\u00ff\u0080\2\u037a\u037c\5\u0103\u0082\2\u037b\u0379")
        buf.write("\3\2\2\2\u037b\u037a\3\2\2\2\u037c\u037f\3\2\2\2\u037d")
        buf.write("\u037b\3\2\2\2\u037d\u037e\3\2\2\2\u037e\u0380\3\2\2\2")
        buf.write("\u037f\u037d\3\2\2\2\u0380\u0382\7$\2\2\u0381\u036f\3")
        buf.write("\2\2\2\u0381\u0378\3\2\2\2\u0382\u00f8\3\2\2\2\u0383\u0384")
        buf.write("\7)\2\2\u0384\u0385\7)\2\2\u0385\u0386\7)\2\2\u0386\u038a")
        buf.write("\3\2\2\2\u0387\u0389\5\u00fb~\2\u0388\u0387\3\2\2\2\u0389")
        buf.write("\u038c\3\2\2\2\u038a\u038b\3\2\2\2\u038a\u0388\3\2\2\2")
        buf.write("\u038b\u038d\3\2\2\2\u038c\u038a\3\2\2\2\u038d\u038e\7")
        buf.write(")\2\2\u038e\u038f\7)\2\2\u038f\u039e\7)\2\2\u0390\u0391")
        buf.write("\7$\2\2\u0391\u0392\7$\2\2\u0392\u0393\7$\2\2\u0393\u0397")
        buf.write("\3\2\2\2\u0394\u0396\5\u00fb~\2\u0395\u0394\3\2\2\2\u0396")
        buf.write("\u0399\3\2\2\2\u0397\u0398\3\2\2\2\u0397\u0395\3\2\2\2")
        buf.write("\u0398\u039a\3\2\2\2\u0399\u0397\3\2\2\2\u039a\u039b\7")
        buf.write("$\2\2\u039b\u039c\7$\2\2\u039c\u039e\7$\2\2\u039d\u0383")
        buf.write("\3\2\2\2\u039d\u0390\3\2\2\2\u039e\u00fa\3\2\2\2\u039f")
        buf.write("\u03a2\5\u0101\u0081\2\u03a0\u03a2\5\u0103\u0082\2\u03a1")
        buf.write("\u039f\3\2\2\2\u03a1\u03a0\3\2\2\2\u03a2\u00fc\3\2\2\2")
        buf.write("\u03a3\u03a5\t\22\2\2\u03a4\u03a3\3\2\2\2\u03a5\u00fe")
        buf.write("\3\2\2\2\u03a6\u03a8\t\23\2\2\u03a7\u03a6\3\2\2\2\u03a8")
        buf.write("\u0100\3\2\2\2\u03a9\u03ab\t\24\2\2\u03aa\u03a9\3\2\2")
        buf.write("\2\u03ab\u0102\3\2\2\2\u03ac\u03ad\7^\2\2\u03ad\u03ae")
        buf.write("\t\25\2\2\u03ae\u0104\3\2\2\2\u03af\u03b1\t\26\2\2\u03b0")
        buf.write("\u03af\3\2\2\2\u03b1\u03b2\3\2\2\2\u03b2\u03b0\3\2\2\2")
        buf.write("\u03b2\u03b3\3\2\2\2\u03b3\u0106\3\2\2\2\u03b4\u03b8\7")
        buf.write("%\2\2\u03b5\u03b7\n\27\2\2\u03b6\u03b5\3\2\2\2\u03b7\u03ba")
        buf.write("\3\2\2\2\u03b8\u03b6\3\2\2\2\u03b8\u03b9\3\2\2\2\u03b9")
        buf.write("\u0108\3\2\2\2\u03ba\u03b8\3\2\2\2\u03bb\u03bd\7^\2\2")
        buf.write("\u03bc\u03be\5\u0105\u0083\2\u03bd\u03bc\3\2\2\2\u03bd")
        buf.write("\u03be\3\2\2\2\u03be\u03c4\3\2\2\2\u03bf\u03c1\7\17\2")
        buf.write("\2\u03c0\u03bf\3\2\2\2\u03c0\u03c1\3\2\2\2\u03c1\u03c2")
        buf.write("\3\2\2\2\u03c2\u03c5\7\f\2\2\u03c3\u03c5\7\17\2\2\u03c4")
        buf.write("\u03c0\3\2\2\2\u03c4\u03c3\3\2\2\2\u03c5\u010a\3\2\2\2")
        buf.write("\u03c6\u03c8\t\30\2\2\u03c7\u03c6\3\2\2\2\u03c8\u010c")
        buf.write("\3\2\2\2\u03c9\u03cc\5\u010b\u0086\2\u03ca\u03cc\t\31")
        buf.write("\2\2\u03cb\u03c9\3\2\2\2\u03cb\u03ca\3\2\2\2\u03cc\u010e")
        buf.write("\3\2\2\29\2\u022c\u0230\u0233\u0235\u023d\u0241\u0244")
        buf.write("\u0248\u024c\u0250\u0256\u025c\u025e\u0265\u026c\u0273")
        buf.write("\u0277\u027b\u0303\u030c\u030e\u0315\u0317\u031b\u0324")
        buf.write("\u0331\u0337\u033b\u034d\u0353\u0357\u035e\u0364\u0368")
        buf.write("\u036d\u0372\u0374\u037b\u037d\u0381\u038a\u0397\u039d")
        buf.write("\u03a1\u03a4\u03a7\u03aa\u03b2\u03b8\u03bd\u03c0\u03c4")
        buf.write("\u03c7\u03cb\n\3\62\2\3?\3\3@\4\3F\5\3G\6\3S\7\3T\b\b")
        buf.write("\2\2")
        return buf.getvalue()


class TaquitoLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    PACKAGE = 1
    ABSTRACT = 2
    EXTENDS = 3
    CONTAINS = 4
    CONTAINER = 5
    OPPOSITE = 6
    REFERS = 7
    END = 8
    DERIVED = 9
    GET = 10
    SET = 11
    ENUM = 12
    ASSOCIATION = 13
    RANGE = 14
    LOWER_RANGE = 15
    DEF = 16
    RETURN = 17
    RAISE = 18
    FROM = 19
    IMPORT = 20
    AS = 21
    GLOBAL = 22
    NONLOCAL = 23
    ASSERT = 24
    IF = 25
    ELIF = 26
    ELSE = 27
    WHILE = 28
    FOR = 29
    IN = 30
    TRY = 31
    FINALLY = 32
    WITH = 33
    EXCEPT = 34
    LAMBDA = 35
    OR = 36
    AND = 37
    NOT = 38
    IS = 39
    NONE = 40
    TRUE = 41
    FALSE = 42
    CLASS = 43
    YIELD = 44
    DEL = 45
    PASS = 46
    CONTINUE = 47
    BREAK = 48
    NEWLINE = 49
    NAME = 50
    STRING_LITERAL = 51
    BYTES_LITERAL = 52
    DECIMAL_INTEGER = 53
    OCT_INTEGER = 54
    HEX_INTEGER = 55
    BIN_INTEGER = 56
    FLOAT_NUMBER = 57
    IMAG_NUMBER = 58
    DOT = 59
    ELLIPSIS = 60
    STAR = 61
    OPEN_PAREN = 62
    CLOSE_PAREN = 63
    COMMA = 64
    COLON = 65
    SEMI_COLON = 66
    POWER = 67
    ASSIGN = 68
    OPEN_BRACK = 69
    CLOSE_BRACK = 70
    OR_OP = 71
    XOR = 72
    AND_OP = 73
    LEFT_SHIFT = 74
    RIGHT_SHIFT = 75
    ADD = 76
    MINUS = 77
    DIV = 78
    MOD = 79
    IDIV = 80
    NOT_OP = 81
    OPEN_BRACE = 82
    CLOSE_BRACE = 83
    LESS_THAN = 84
    GREATER_THAN = 85
    EQUALS = 86
    GT_EQ = 87
    LT_EQ = 88
    NOT_EQ_1 = 89
    NOT_EQ_2 = 90
    AT = 91
    ARROW = 92
    ADD_ASSIGN = 93
    SUB_ASSIGN = 94
    MULT_ASSIGN = 95
    AT_ASSIGN = 96
    DIV_ASSIGN = 97
    MOD_ASSIGN = 98
    AND_ASSIGN = 99
    OR_ASSIGN = 100
    XOR_ASSIGN = 101
    LEFT_SHIFT_ASSIGN = 102
    RIGHT_SHIFT_ASSIGN = 103
    POWER_ASSIGN = 104
    IDIV_ASSIGN = 105
    SKIP_ = 106
    UNKNOWN_CHAR = 107

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'package'", "'abstract'", "'extends'", "'contains'", "'container'", 
            "'opposite'", "'refers'", "'end'", "'derived'", "'get'", "'set'", 
            "'enum'", "'association'", "'..'", "'def'", "'return'", "'raise'", 
            "'from'", "'import'", "'as'", "'global'", "'nonlocal'", "'assert'", 
            "'if'", "'elif'", "'else'", "'while'", "'for'", "'in'", "'try'", 
            "'finally'", "'with'", "'except'", "'lambda'", "'or'", "'and'", 
            "'not'", "'is'", "'None'", "'True'", "'False'", "'class'", "'yield'", 
            "'del'", "'pass'", "'continue'", "'break'", "'.'", "'...'", 
            "'*'", "'('", "')'", "','", "':'", "';'", "'**'", "'='", "'['", 
            "']'", "'|'", "'^'", "'&'", "'<<'", "'>>'", "'+'", "'-'", "'/'", 
            "'%'", "'//'", "'~'", "'{'", "'}'", "'<'", "'>'", "'=='", "'>='", 
            "'<='", "'<>'", "'!='", "'@'", "'->'", "'+='", "'-='", "'*='", 
            "'@='", "'/='", "'%='", "'&='", "'|='", "'^='", "'<<='", "'>>='", 
            "'**='", "'//='" ]

    symbolicNames = [ "<INVALID>",
            "PACKAGE", "ABSTRACT", "EXTENDS", "CONTAINS", "CONTAINER", "OPPOSITE", 
            "REFERS", "END", "DERIVED", "GET", "SET", "ENUM", "ASSOCIATION", 
            "RANGE", "LOWER_RANGE", "DEF", "RETURN", "RAISE", "FROM", "IMPORT", 
            "AS", "GLOBAL", "NONLOCAL", "ASSERT", "IF", "ELIF", "ELSE", 
            "WHILE", "FOR", "IN", "TRY", "FINALLY", "WITH", "EXCEPT", "LAMBDA", 
            "OR", "AND", "NOT", "IS", "NONE", "TRUE", "FALSE", "CLASS", 
            "YIELD", "DEL", "PASS", "CONTINUE", "BREAK", "NEWLINE", "NAME", 
            "STRING_LITERAL", "BYTES_LITERAL", "DECIMAL_INTEGER", "OCT_INTEGER", 
            "HEX_INTEGER", "BIN_INTEGER", "FLOAT_NUMBER", "IMAG_NUMBER", 
            "DOT", "ELLIPSIS", "STAR", "OPEN_PAREN", "CLOSE_PAREN", "COMMA", 
            "COLON", "SEMI_COLON", "POWER", "ASSIGN", "OPEN_BRACK", "CLOSE_BRACK", 
            "OR_OP", "XOR", "AND_OP", "LEFT_SHIFT", "RIGHT_SHIFT", "ADD", 
            "MINUS", "DIV", "MOD", "IDIV", "NOT_OP", "OPEN_BRACE", "CLOSE_BRACE", 
            "LESS_THAN", "GREATER_THAN", "EQUALS", "GT_EQ", "LT_EQ", "NOT_EQ_1", 
            "NOT_EQ_2", "AT", "ARROW", "ADD_ASSIGN", "SUB_ASSIGN", "MULT_ASSIGN", 
            "AT_ASSIGN", "DIV_ASSIGN", "MOD_ASSIGN", "AND_ASSIGN", "OR_ASSIGN", 
            "XOR_ASSIGN", "LEFT_SHIFT_ASSIGN", "RIGHT_SHIFT_ASSIGN", "POWER_ASSIGN", 
            "IDIV_ASSIGN", "SKIP_", "UNKNOWN_CHAR" ]

    ruleNames = [ "PACKAGE", "ABSTRACT", "EXTENDS", "CONTAINS", "CONTAINER", 
                  "OPPOSITE", "REFERS", "END", "DERIVED", "GET", "SET", 
                  "ENUM", "ASSOCIATION", "RANGE", "LOWER_RANGE", "DEF", 
                  "RETURN", "RAISE", "FROM", "IMPORT", "AS", "GLOBAL", "NONLOCAL", 
                  "ASSERT", "IF", "ELIF", "ELSE", "WHILE", "FOR", "IN", 
                  "TRY", "FINALLY", "WITH", "EXCEPT", "LAMBDA", "OR", "AND", 
                  "NOT", "IS", "NONE", "TRUE", "FALSE", "CLASS", "YIELD", 
                  "DEL", "PASS", "CONTINUE", "BREAK", "NEWLINE", "NAME", 
                  "STRING_LITERAL", "BYTES_LITERAL", "DECIMAL_INTEGER", 
                  "OCT_INTEGER", "HEX_INTEGER", "BIN_INTEGER", "FLOAT_NUMBER", 
                  "IMAG_NUMBER", "DOT", "ELLIPSIS", "STAR", "OPEN_PAREN", 
                  "CLOSE_PAREN", "COMMA", "COLON", "SEMI_COLON", "POWER", 
                  "ASSIGN", "OPEN_BRACK", "CLOSE_BRACK", "OR_OP", "XOR", 
                  "AND_OP", "LEFT_SHIFT", "RIGHT_SHIFT", "ADD", "MINUS", 
                  "DIV", "MOD", "IDIV", "NOT_OP", "OPEN_BRACE", "CLOSE_BRACE", 
                  "LESS_THAN", "GREATER_THAN", "EQUALS", "GT_EQ", "LT_EQ", 
                  "NOT_EQ_1", "NOT_EQ_2", "AT", "ARROW", "ADD_ASSIGN", "SUB_ASSIGN", 
                  "MULT_ASSIGN", "AT_ASSIGN", "DIV_ASSIGN", "MOD_ASSIGN", 
                  "AND_ASSIGN", "OR_ASSIGN", "XOR_ASSIGN", "LEFT_SHIFT_ASSIGN", 
                  "RIGHT_SHIFT_ASSIGN", "POWER_ASSIGN", "IDIV_ASSIGN", "SKIP_", 
                  "UNKNOWN_CHAR", "SHORT_STRING", "LONG_STRING", "LONG_STRING_ITEM", 
                  "LONG_STRING_CHAR", "STRING_ESCAPE_SEQ", "NON_ZERO_DIGIT", 
                  "DIGIT", "OCT_DIGIT", "HEX_DIGIT", "BIN_DIGIT", "POINT_FLOAT", 
                  "EXPONENT_FLOAT", "INT_PART", "FRACTION", "EXPONENT", 
                  "SHORT_BYTES", "LONG_BYTES", "LONG_BYTES_ITEM", "SHORT_BYTES_CHAR_NO_SINGLE_QUOTE", 
                  "SHORT_BYTES_CHAR_NO_DOUBLE_QUOTE", "LONG_BYTES_CHAR", 
                  "BYTES_ESCAPE_SEQ", "SPACES", "COMMENT", "LINE_JOINING", 
                  "ID_START", "ID_CONTINUE" ]

    grammarFileName = "Taquito.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    @property
    def tokens(self):
        try:
            return self._tokens
        except AttributeError:
            self._tokens = []
            return self._tokens

    @property
    def indents(self):
        try:
            return self._indents
        except AttributeError:
            self._indents = []
            return self._indents

    @property
    def opened(self):
        try:
            return self._opened
        except AttributeError:
            self._opened = 0
            return self._opened

    @opened.setter
    def opened(self, value):
        self._opened = value

    @property
    def lastToken(self):
        try:
            return self._lastToken
        except AttributeError:
            self._lastToken = None
            return self._lastToken

    @lastToken.setter
    def lastToken(self, value):
        self._lastToken = value

    def reset(self):
        super().reset()
        self.tokens = []
        self.indents = []
        self.opened = 0
        self.lastToken = None

    def emitToken(self, t):
        super().emitToken(t)
        self.tokens.append(t)

    def nextToken(self):
        if self._input.LA(1) == Token.EOF and self.indents:
            for i in range(len(self.tokens)-1,-1,-1):
                if self.tokens[i].type == Token.EOF:
                    self.tokens.pop(i)

            self.emitToken(self.commonToken(LanguageParser.NEWLINE, '\n'))
            while self.indents:
                self.emitToken(self.createDedent())
                self.indents.pop()

            self.emitToken(self.commonToken(LanguageParser.EOF, "<EOF>"))
        next = super().nextToken()
        if next.channel == Token.DEFAULT_CHANNEL:
            self.lastToken = next
        return next if not self.tokens else self.tokens.pop(0)

    def createDedent(self):
        dedent = self.commonToken(LanguageParser.DEDENT, "")
        dedent.line = self.lastToken.line
        return dedent

    def commonToken(self, type, text, indent=0):
        stop = self.getCharIndex()-1-indent
        start = (stop - len(text) + 1) if text else stop
        return CommonToken(self._tokenFactorySourcePair, type, super().DEFAULT_TOKEN_CHANNEL, start, stop)

    @staticmethod
    def getIndentationCount(spaces):
        count = 0
        for ch in spaces:
            if ch == '\t':
                count += 8 - (count % 8)
            else:
                count += 1
        return count

    def atStartOfInput(self):
        return Lexer.column.fget(self) == 0 and Lexer.line.fget(self) == 1


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
    	if self._actions is None:
    		actions = dict()
    		actions[48] = self.NEWLINE_action 
    		actions[61] = self.OPEN_PAREN_action 
    		actions[62] = self.CLOSE_PAREN_action 
    		actions[68] = self.OPEN_BRACK_action 
    		actions[69] = self.CLOSE_BRACK_action 
    		actions[81] = self.OPEN_BRACE_action 
    		actions[82] = self.CLOSE_BRACE_action 
    		self._actions = actions
    	action = self._actions.get(ruleIndex, None)
    	if action is not None:
    		action(localctx, actionIndex)
    	else:
    		raise Exception("No registered action for:" + str(ruleIndex))

    def NEWLINE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

            tempt = Lexer.text.fget(self)
            newLine = re.sub("[^\r\n]+", "", tempt)
            spaces = re.sub("[\r\n]+", "", tempt)
            la_char = ""
            try:
                la = self._input.LA(1)
                la_char = chr(la)       # Python does not compare char to ints directly
            except ValueError:          # End of file
                pass

            if self.opened > 0 or la_char == '\r' or la_char == '\n' or la_char == '#':
                self.skip()
            else:
                indent = self.getIndentationCount(spaces)
                previous = self.indents[-1] if self.indents else 0
                self.emitToken(self.commonToken(self.NEWLINE, newLine, indent=indent))      # NEWLINE is actually the '\n' char
                if indent == previous:
                    self.skip()
                elif indent > previous:
                    self.indents.append(indent)
                    self.emitToken(self.commonToken(LanguageParser.INDENT, spaces))
                else:
                    while self.indents and self.indents[-1] > indent:
                        self.emitToken(self.createDedent())
                        self.indents.pop()
                
     

    def OPEN_PAREN_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:
            self.opened += 1
     

    def CLOSE_PAREN_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:
            self.opened -= 1
     

    def OPEN_BRACK_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
            self.opened += 1
     

    def CLOSE_BRACK_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 4:
            self.opened -= 1
     

    def OPEN_BRACE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 5:
            self.opened += 1
     

    def CLOSE_BRACE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 6:
            self.opened -= 1
     

    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates is None:
            preds = dict()
            preds[48] = self.NEWLINE_sempred
            self._predicates = preds
        pred = self._predicates.get(ruleIndex, None)
        if pred is not None:
            return pred(localctx, predIndex)
        else:
            raise Exception("No registered predicate for:" + str(ruleIndex))

    def NEWLINE_sempred(self, localctx:RuleContext, predIndex:int):
            if predIndex == 0:
                return self.atStartOfInput()
         


