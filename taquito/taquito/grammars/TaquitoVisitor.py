# Generated from /Users/horacio/Git/taquito/taquito/antlr/Taquito.g4 by ANTLR 4.7
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TaquitoParser import TaquitoParser
else:
    from TaquitoParser import TaquitoParser

# This class defines a complete generic visitor for a parse tree produced by TaquitoParser.

class TaquitoVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by TaquitoParser#file_input.
    def visitFile_input(self, ctx:TaquitoParser.File_inputContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_packdef.
    def visitT_packdef(self, ctx:TaquitoParser.T_packdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_package_stmt.
    def visitT_package_stmt(self, ctx:TaquitoParser.T_package_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_classdef.
    def visitT_classdef(self, ctx:TaquitoParser.T_classdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_class_suite.
    def visitT_class_suite(self, ctx:TaquitoParser.T_class_suiteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_class_simple_stmt.
    def visitT_class_simple_stmt(self, ctx:TaquitoParser.T_class_simple_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_class_small_stmt.
    def visitT_class_small_stmt(self, ctx:TaquitoParser.T_class_small_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_class_stmt.
    def visitT_class_stmt(self, ctx:TaquitoParser.T_class_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_class_compound_stmt.
    def visitT_class_compound_stmt(self, ctx:TaquitoParser.T_class_compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#feat_stmt.
    def visitFeat_stmt(self, ctx:TaquitoParser.Feat_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat.
    def visitDerived_feat(self, ctx:TaquitoParser.Derived_featContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_suite.
    def visitDerived_feat_suite(self, ctx:TaquitoParser.Derived_feat_suiteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_stmt.
    def visitDerived_feat_stmt(self, ctx:TaquitoParser.Derived_feat_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_simple_stmt.
    def visitDerived_feat_simple_stmt(self, ctx:TaquitoParser.Derived_feat_simple_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_small_stmt.
    def visitDerived_feat_small_stmt(self, ctx:TaquitoParser.Derived_feat_small_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_compound_stmt.
    def visitDerived_feat_compound_stmt(self, ctx:TaquitoParser.Derived_feat_compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_get_stmt.
    def visitDerived_feat_get_stmt(self, ctx:TaquitoParser.Derived_feat_get_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_set_stmt.
    def visitDerived_feat_set_stmt(self, ctx:TaquitoParser.Derived_feat_set_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#derived_feat_xet_stmt.
    def visitDerived_feat_xet_stmt(self, ctx:TaquitoParser.Derived_feat_xet_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_multiplicity.
    def visitT_multiplicity(self, ctx:TaquitoParser.T_multiplicityContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_multi_range.
    def visitT_multi_range(self, ctx:TaquitoParser.T_multi_rangeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#ref_stmt.
    def visitRef_stmt(self, ctx:TaquitoParser.Ref_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#attr_stmt.
    def visitAttr_stmt(self, ctx:TaquitoParser.Attr_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_extends_stmt.
    def visitT_extends_stmt(self, ctx:TaquitoParser.T_extends_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_enum_stmt.
    def visitT_enum_stmt(self, ctx:TaquitoParser.T_enum_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_simple_enum_stmt.
    def visitT_simple_enum_stmt(self, ctx:TaquitoParser.T_simple_enum_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_small_enum_stmt.
    def visitT_small_enum_stmt(self, ctx:TaquitoParser.T_small_enum_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_enum_expr_stmt.
    def visitT_enum_expr_stmt(self, ctx:TaquitoParser.T_enum_expr_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_enum_value.
    def visitT_enum_value(self, ctx:TaquitoParser.T_enum_valueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_enumdef_suite.
    def visitT_enumdef_suite(self, ctx:TaquitoParser.T_enumdef_suiteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_enumdef.
    def visitT_enumdef(self, ctx:TaquitoParser.T_enumdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assocdef.
    def visitT_assocdef(self, ctx:TaquitoParser.T_assocdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assoc_suite.
    def visitT_assoc_suite(self, ctx:TaquitoParser.T_assoc_suiteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assoc_simple_stmt.
    def visitT_assoc_simple_stmt(self, ctx:TaquitoParser.T_assoc_simple_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assoc_small_stmt.
    def visitT_assoc_small_stmt(self, ctx:TaquitoParser.T_assoc_small_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assoc_stmt.
    def visitT_assoc_stmt(self, ctx:TaquitoParser.T_assoc_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#t_assoc_compound_stmt.
    def visitT_assoc_compound_stmt(self, ctx:TaquitoParser.T_assoc_compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#assoc_classifier_end.
    def visitAssoc_classifier_end(self, ctx:TaquitoParser.Assoc_classifier_endContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#single_input.
    def visitSingle_input(self, ctx:TaquitoParser.Single_inputContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#eval_input.
    def visitEval_input(self, ctx:TaquitoParser.Eval_inputContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#decorator.
    def visitDecorator(self, ctx:TaquitoParser.DecoratorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#decorators.
    def visitDecorators(self, ctx:TaquitoParser.DecoratorsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#decorated.
    def visitDecorated(self, ctx:TaquitoParser.DecoratedContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#funcdef.
    def visitFuncdef(self, ctx:TaquitoParser.FuncdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#parameters.
    def visitParameters(self, ctx:TaquitoParser.ParametersContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#typedargslist.
    def visitTypedargslist(self, ctx:TaquitoParser.TypedargslistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#tfpdef.
    def visitTfpdef(self, ctx:TaquitoParser.TfpdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#varargslist.
    def visitVarargslist(self, ctx:TaquitoParser.VarargslistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#vfpdef.
    def visitVfpdef(self, ctx:TaquitoParser.VfpdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#stmt.
    def visitStmt(self, ctx:TaquitoParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#simple_stmt.
    def visitSimple_stmt(self, ctx:TaquitoParser.Simple_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#small_stmt.
    def visitSmall_stmt(self, ctx:TaquitoParser.Small_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#expr_stmt.
    def visitExpr_stmt(self, ctx:TaquitoParser.Expr_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#testlist_star_expr.
    def visitTestlist_star_expr(self, ctx:TaquitoParser.Testlist_star_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#augassign.
    def visitAugassign(self, ctx:TaquitoParser.AugassignContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#del_stmt.
    def visitDel_stmt(self, ctx:TaquitoParser.Del_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#pass_stmt.
    def visitPass_stmt(self, ctx:TaquitoParser.Pass_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#flow_stmt.
    def visitFlow_stmt(self, ctx:TaquitoParser.Flow_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#break_stmt.
    def visitBreak_stmt(self, ctx:TaquitoParser.Break_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#continue_stmt.
    def visitContinue_stmt(self, ctx:TaquitoParser.Continue_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#return_stmt.
    def visitReturn_stmt(self, ctx:TaquitoParser.Return_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#yield_stmt.
    def visitYield_stmt(self, ctx:TaquitoParser.Yield_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#raise_stmt.
    def visitRaise_stmt(self, ctx:TaquitoParser.Raise_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#import_stmt.
    def visitImport_stmt(self, ctx:TaquitoParser.Import_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#import_name.
    def visitImport_name(self, ctx:TaquitoParser.Import_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#import_from.
    def visitImport_from(self, ctx:TaquitoParser.Import_fromContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#import_as_name.
    def visitImport_as_name(self, ctx:TaquitoParser.Import_as_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#dotted_as_name.
    def visitDotted_as_name(self, ctx:TaquitoParser.Dotted_as_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#import_as_names.
    def visitImport_as_names(self, ctx:TaquitoParser.Import_as_namesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#dotted_as_names.
    def visitDotted_as_names(self, ctx:TaquitoParser.Dotted_as_namesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#dotted_name.
    def visitDotted_name(self, ctx:TaquitoParser.Dotted_nameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#global_stmt.
    def visitGlobal_stmt(self, ctx:TaquitoParser.Global_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#nonlocal_stmt.
    def visitNonlocal_stmt(self, ctx:TaquitoParser.Nonlocal_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#assert_stmt.
    def visitAssert_stmt(self, ctx:TaquitoParser.Assert_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#compound_stmt.
    def visitCompound_stmt(self, ctx:TaquitoParser.Compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#if_stmt.
    def visitIf_stmt(self, ctx:TaquitoParser.If_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#while_stmt.
    def visitWhile_stmt(self, ctx:TaquitoParser.While_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#for_stmt.
    def visitFor_stmt(self, ctx:TaquitoParser.For_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#try_stmt.
    def visitTry_stmt(self, ctx:TaquitoParser.Try_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#with_stmt.
    def visitWith_stmt(self, ctx:TaquitoParser.With_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#with_item.
    def visitWith_item(self, ctx:TaquitoParser.With_itemContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#except_clause.
    def visitExcept_clause(self, ctx:TaquitoParser.Except_clauseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#suite.
    def visitSuite(self, ctx:TaquitoParser.SuiteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#test.
    def visitTest(self, ctx:TaquitoParser.TestContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#test_nocond.
    def visitTest_nocond(self, ctx:TaquitoParser.Test_nocondContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#lambdef.
    def visitLambdef(self, ctx:TaquitoParser.LambdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#lambdef_nocond.
    def visitLambdef_nocond(self, ctx:TaquitoParser.Lambdef_nocondContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#or_test.
    def visitOr_test(self, ctx:TaquitoParser.Or_testContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#and_test.
    def visitAnd_test(self, ctx:TaquitoParser.And_testContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#not_test.
    def visitNot_test(self, ctx:TaquitoParser.Not_testContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#comparison.
    def visitComparison(self, ctx:TaquitoParser.ComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#comp_op.
    def visitComp_op(self, ctx:TaquitoParser.Comp_opContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#star_expr.
    def visitStar_expr(self, ctx:TaquitoParser.Star_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#expr.
    def visitExpr(self, ctx:TaquitoParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#xor_expr.
    def visitXor_expr(self, ctx:TaquitoParser.Xor_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#and_expr.
    def visitAnd_expr(self, ctx:TaquitoParser.And_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#shift_expr.
    def visitShift_expr(self, ctx:TaquitoParser.Shift_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#arith_expr.
    def visitArith_expr(self, ctx:TaquitoParser.Arith_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#term.
    def visitTerm(self, ctx:TaquitoParser.TermContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#factor.
    def visitFactor(self, ctx:TaquitoParser.FactorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#power.
    def visitPower(self, ctx:TaquitoParser.PowerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#atom.
    def visitAtom(self, ctx:TaquitoParser.AtomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#testlist_comp.
    def visitTestlist_comp(self, ctx:TaquitoParser.Testlist_compContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#trailer.
    def visitTrailer(self, ctx:TaquitoParser.TrailerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#subscriptlist.
    def visitSubscriptlist(self, ctx:TaquitoParser.SubscriptlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#subscript.
    def visitSubscript(self, ctx:TaquitoParser.SubscriptContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#sliceop.
    def visitSliceop(self, ctx:TaquitoParser.SliceopContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#exprlist.
    def visitExprlist(self, ctx:TaquitoParser.ExprlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#testlist.
    def visitTestlist(self, ctx:TaquitoParser.TestlistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#dictorsetmaker.
    def visitDictorsetmaker(self, ctx:TaquitoParser.DictorsetmakerContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#classdef.
    def visitClassdef(self, ctx:TaquitoParser.ClassdefContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#arglist.
    def visitArglist(self, ctx:TaquitoParser.ArglistContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#argument.
    def visitArgument(self, ctx:TaquitoParser.ArgumentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#comp_iter.
    def visitComp_iter(self, ctx:TaquitoParser.Comp_iterContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#comp_for.
    def visitComp_for(self, ctx:TaquitoParser.Comp_forContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#comp_if.
    def visitComp_if(self, ctx:TaquitoParser.Comp_ifContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#yield_expr.
    def visitYield_expr(self, ctx:TaquitoParser.Yield_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#yield_arg.
    def visitYield_arg(self, ctx:TaquitoParser.Yield_argContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#strr.
    def visitStrr(self, ctx:TaquitoParser.StrrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#number.
    def visitNumber(self, ctx:TaquitoParser.NumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TaquitoParser#integer.
    def visitInteger(self, ctx:TaquitoParser.IntegerContext):
        return self.visitChildren(ctx)



del TaquitoParser