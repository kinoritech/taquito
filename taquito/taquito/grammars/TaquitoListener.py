# Generated from /Users/horacio/Git/taquito/taquito/antlr/Taquito.g4 by ANTLR 4.7
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TaquitoParser import TaquitoParser
else:
    from TaquitoParser import TaquitoParser

# This class defines a complete listener for a parse tree produced by TaquitoParser.
class TaquitoListener(ParseTreeListener):

    # Enter a parse tree produced by TaquitoParser#file_input.
    def enterFile_input(self, ctx:TaquitoParser.File_inputContext):
        pass

    # Exit a parse tree produced by TaquitoParser#file_input.
    def exitFile_input(self, ctx:TaquitoParser.File_inputContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_packdef.
    def enterT_packdef(self, ctx:TaquitoParser.T_packdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_packdef.
    def exitT_packdef(self, ctx:TaquitoParser.T_packdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_package_stmt.
    def enterT_package_stmt(self, ctx:TaquitoParser.T_package_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_package_stmt.
    def exitT_package_stmt(self, ctx:TaquitoParser.T_package_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_classdef.
    def enterT_classdef(self, ctx:TaquitoParser.T_classdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_classdef.
    def exitT_classdef(self, ctx:TaquitoParser.T_classdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_class_suite.
    def enterT_class_suite(self, ctx:TaquitoParser.T_class_suiteContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_class_suite.
    def exitT_class_suite(self, ctx:TaquitoParser.T_class_suiteContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_class_simple_stmt.
    def enterT_class_simple_stmt(self, ctx:TaquitoParser.T_class_simple_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_class_simple_stmt.
    def exitT_class_simple_stmt(self, ctx:TaquitoParser.T_class_simple_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_class_small_stmt.
    def enterT_class_small_stmt(self, ctx:TaquitoParser.T_class_small_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_class_small_stmt.
    def exitT_class_small_stmt(self, ctx:TaquitoParser.T_class_small_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_class_stmt.
    def enterT_class_stmt(self, ctx:TaquitoParser.T_class_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_class_stmt.
    def exitT_class_stmt(self, ctx:TaquitoParser.T_class_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_class_compound_stmt.
    def enterT_class_compound_stmt(self, ctx:TaquitoParser.T_class_compound_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_class_compound_stmt.
    def exitT_class_compound_stmt(self, ctx:TaquitoParser.T_class_compound_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#feat_stmt.
    def enterFeat_stmt(self, ctx:TaquitoParser.Feat_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#feat_stmt.
    def exitFeat_stmt(self, ctx:TaquitoParser.Feat_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat.
    def enterDerived_feat(self, ctx:TaquitoParser.Derived_featContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat.
    def exitDerived_feat(self, ctx:TaquitoParser.Derived_featContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_suite.
    def enterDerived_feat_suite(self, ctx:TaquitoParser.Derived_feat_suiteContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_suite.
    def exitDerived_feat_suite(self, ctx:TaquitoParser.Derived_feat_suiteContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_stmt.
    def enterDerived_feat_stmt(self, ctx:TaquitoParser.Derived_feat_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_stmt.
    def exitDerived_feat_stmt(self, ctx:TaquitoParser.Derived_feat_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_simple_stmt.
    def enterDerived_feat_simple_stmt(self, ctx:TaquitoParser.Derived_feat_simple_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_simple_stmt.
    def exitDerived_feat_simple_stmt(self, ctx:TaquitoParser.Derived_feat_simple_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_small_stmt.
    def enterDerived_feat_small_stmt(self, ctx:TaquitoParser.Derived_feat_small_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_small_stmt.
    def exitDerived_feat_small_stmt(self, ctx:TaquitoParser.Derived_feat_small_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_compound_stmt.
    def enterDerived_feat_compound_stmt(self, ctx:TaquitoParser.Derived_feat_compound_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_compound_stmt.
    def exitDerived_feat_compound_stmt(self, ctx:TaquitoParser.Derived_feat_compound_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_get_stmt.
    def enterDerived_feat_get_stmt(self, ctx:TaquitoParser.Derived_feat_get_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_get_stmt.
    def exitDerived_feat_get_stmt(self, ctx:TaquitoParser.Derived_feat_get_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_set_stmt.
    def enterDerived_feat_set_stmt(self, ctx:TaquitoParser.Derived_feat_set_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_set_stmt.
    def exitDerived_feat_set_stmt(self, ctx:TaquitoParser.Derived_feat_set_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#derived_feat_xet_stmt.
    def enterDerived_feat_xet_stmt(self, ctx:TaquitoParser.Derived_feat_xet_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#derived_feat_xet_stmt.
    def exitDerived_feat_xet_stmt(self, ctx:TaquitoParser.Derived_feat_xet_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_multiplicity.
    def enterT_multiplicity(self, ctx:TaquitoParser.T_multiplicityContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_multiplicity.
    def exitT_multiplicity(self, ctx:TaquitoParser.T_multiplicityContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_multi_range.
    def enterT_multi_range(self, ctx:TaquitoParser.T_multi_rangeContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_multi_range.
    def exitT_multi_range(self, ctx:TaquitoParser.T_multi_rangeContext):
        pass


    # Enter a parse tree produced by TaquitoParser#ref_stmt.
    def enterRef_stmt(self, ctx:TaquitoParser.Ref_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#ref_stmt.
    def exitRef_stmt(self, ctx:TaquitoParser.Ref_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#attr_stmt.
    def enterAttr_stmt(self, ctx:TaquitoParser.Attr_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#attr_stmt.
    def exitAttr_stmt(self, ctx:TaquitoParser.Attr_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_extends_stmt.
    def enterT_extends_stmt(self, ctx:TaquitoParser.T_extends_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_extends_stmt.
    def exitT_extends_stmt(self, ctx:TaquitoParser.T_extends_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_enum_stmt.
    def enterT_enum_stmt(self, ctx:TaquitoParser.T_enum_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_enum_stmt.
    def exitT_enum_stmt(self, ctx:TaquitoParser.T_enum_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_simple_enum_stmt.
    def enterT_simple_enum_stmt(self, ctx:TaquitoParser.T_simple_enum_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_simple_enum_stmt.
    def exitT_simple_enum_stmt(self, ctx:TaquitoParser.T_simple_enum_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_small_enum_stmt.
    def enterT_small_enum_stmt(self, ctx:TaquitoParser.T_small_enum_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_small_enum_stmt.
    def exitT_small_enum_stmt(self, ctx:TaquitoParser.T_small_enum_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_enum_expr_stmt.
    def enterT_enum_expr_stmt(self, ctx:TaquitoParser.T_enum_expr_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_enum_expr_stmt.
    def exitT_enum_expr_stmt(self, ctx:TaquitoParser.T_enum_expr_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_enum_value.
    def enterT_enum_value(self, ctx:TaquitoParser.T_enum_valueContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_enum_value.
    def exitT_enum_value(self, ctx:TaquitoParser.T_enum_valueContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_enumdef_suite.
    def enterT_enumdef_suite(self, ctx:TaquitoParser.T_enumdef_suiteContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_enumdef_suite.
    def exitT_enumdef_suite(self, ctx:TaquitoParser.T_enumdef_suiteContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_enumdef.
    def enterT_enumdef(self, ctx:TaquitoParser.T_enumdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_enumdef.
    def exitT_enumdef(self, ctx:TaquitoParser.T_enumdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assocdef.
    def enterT_assocdef(self, ctx:TaquitoParser.T_assocdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assocdef.
    def exitT_assocdef(self, ctx:TaquitoParser.T_assocdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assoc_suite.
    def enterT_assoc_suite(self, ctx:TaquitoParser.T_assoc_suiteContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assoc_suite.
    def exitT_assoc_suite(self, ctx:TaquitoParser.T_assoc_suiteContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assoc_simple_stmt.
    def enterT_assoc_simple_stmt(self, ctx:TaquitoParser.T_assoc_simple_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assoc_simple_stmt.
    def exitT_assoc_simple_stmt(self, ctx:TaquitoParser.T_assoc_simple_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assoc_small_stmt.
    def enterT_assoc_small_stmt(self, ctx:TaquitoParser.T_assoc_small_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assoc_small_stmt.
    def exitT_assoc_small_stmt(self, ctx:TaquitoParser.T_assoc_small_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assoc_stmt.
    def enterT_assoc_stmt(self, ctx:TaquitoParser.T_assoc_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assoc_stmt.
    def exitT_assoc_stmt(self, ctx:TaquitoParser.T_assoc_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#t_assoc_compound_stmt.
    def enterT_assoc_compound_stmt(self, ctx:TaquitoParser.T_assoc_compound_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#t_assoc_compound_stmt.
    def exitT_assoc_compound_stmt(self, ctx:TaquitoParser.T_assoc_compound_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#assoc_classifier_end.
    def enterAssoc_classifier_end(self, ctx:TaquitoParser.Assoc_classifier_endContext):
        pass

    # Exit a parse tree produced by TaquitoParser#assoc_classifier_end.
    def exitAssoc_classifier_end(self, ctx:TaquitoParser.Assoc_classifier_endContext):
        pass


    # Enter a parse tree produced by TaquitoParser#single_input.
    def enterSingle_input(self, ctx:TaquitoParser.Single_inputContext):
        pass

    # Exit a parse tree produced by TaquitoParser#single_input.
    def exitSingle_input(self, ctx:TaquitoParser.Single_inputContext):
        pass


    # Enter a parse tree produced by TaquitoParser#eval_input.
    def enterEval_input(self, ctx:TaquitoParser.Eval_inputContext):
        pass

    # Exit a parse tree produced by TaquitoParser#eval_input.
    def exitEval_input(self, ctx:TaquitoParser.Eval_inputContext):
        pass


    # Enter a parse tree produced by TaquitoParser#decorator.
    def enterDecorator(self, ctx:TaquitoParser.DecoratorContext):
        pass

    # Exit a parse tree produced by TaquitoParser#decorator.
    def exitDecorator(self, ctx:TaquitoParser.DecoratorContext):
        pass


    # Enter a parse tree produced by TaquitoParser#decorators.
    def enterDecorators(self, ctx:TaquitoParser.DecoratorsContext):
        pass

    # Exit a parse tree produced by TaquitoParser#decorators.
    def exitDecorators(self, ctx:TaquitoParser.DecoratorsContext):
        pass


    # Enter a parse tree produced by TaquitoParser#decorated.
    def enterDecorated(self, ctx:TaquitoParser.DecoratedContext):
        pass

    # Exit a parse tree produced by TaquitoParser#decorated.
    def exitDecorated(self, ctx:TaquitoParser.DecoratedContext):
        pass


    # Enter a parse tree produced by TaquitoParser#funcdef.
    def enterFuncdef(self, ctx:TaquitoParser.FuncdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#funcdef.
    def exitFuncdef(self, ctx:TaquitoParser.FuncdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#parameters.
    def enterParameters(self, ctx:TaquitoParser.ParametersContext):
        pass

    # Exit a parse tree produced by TaquitoParser#parameters.
    def exitParameters(self, ctx:TaquitoParser.ParametersContext):
        pass


    # Enter a parse tree produced by TaquitoParser#typedargslist.
    def enterTypedargslist(self, ctx:TaquitoParser.TypedargslistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#typedargslist.
    def exitTypedargslist(self, ctx:TaquitoParser.TypedargslistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#tfpdef.
    def enterTfpdef(self, ctx:TaquitoParser.TfpdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#tfpdef.
    def exitTfpdef(self, ctx:TaquitoParser.TfpdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#varargslist.
    def enterVarargslist(self, ctx:TaquitoParser.VarargslistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#varargslist.
    def exitVarargslist(self, ctx:TaquitoParser.VarargslistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#vfpdef.
    def enterVfpdef(self, ctx:TaquitoParser.VfpdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#vfpdef.
    def exitVfpdef(self, ctx:TaquitoParser.VfpdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#stmt.
    def enterStmt(self, ctx:TaquitoParser.StmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#stmt.
    def exitStmt(self, ctx:TaquitoParser.StmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#simple_stmt.
    def enterSimple_stmt(self, ctx:TaquitoParser.Simple_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#simple_stmt.
    def exitSimple_stmt(self, ctx:TaquitoParser.Simple_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#small_stmt.
    def enterSmall_stmt(self, ctx:TaquitoParser.Small_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#small_stmt.
    def exitSmall_stmt(self, ctx:TaquitoParser.Small_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#expr_stmt.
    def enterExpr_stmt(self, ctx:TaquitoParser.Expr_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#expr_stmt.
    def exitExpr_stmt(self, ctx:TaquitoParser.Expr_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#testlist_star_expr.
    def enterTestlist_star_expr(self, ctx:TaquitoParser.Testlist_star_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#testlist_star_expr.
    def exitTestlist_star_expr(self, ctx:TaquitoParser.Testlist_star_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#augassign.
    def enterAugassign(self, ctx:TaquitoParser.AugassignContext):
        pass

    # Exit a parse tree produced by TaquitoParser#augassign.
    def exitAugassign(self, ctx:TaquitoParser.AugassignContext):
        pass


    # Enter a parse tree produced by TaquitoParser#del_stmt.
    def enterDel_stmt(self, ctx:TaquitoParser.Del_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#del_stmt.
    def exitDel_stmt(self, ctx:TaquitoParser.Del_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#pass_stmt.
    def enterPass_stmt(self, ctx:TaquitoParser.Pass_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#pass_stmt.
    def exitPass_stmt(self, ctx:TaquitoParser.Pass_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#flow_stmt.
    def enterFlow_stmt(self, ctx:TaquitoParser.Flow_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#flow_stmt.
    def exitFlow_stmt(self, ctx:TaquitoParser.Flow_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#break_stmt.
    def enterBreak_stmt(self, ctx:TaquitoParser.Break_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#break_stmt.
    def exitBreak_stmt(self, ctx:TaquitoParser.Break_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#continue_stmt.
    def enterContinue_stmt(self, ctx:TaquitoParser.Continue_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#continue_stmt.
    def exitContinue_stmt(self, ctx:TaquitoParser.Continue_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#return_stmt.
    def enterReturn_stmt(self, ctx:TaquitoParser.Return_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#return_stmt.
    def exitReturn_stmt(self, ctx:TaquitoParser.Return_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#yield_stmt.
    def enterYield_stmt(self, ctx:TaquitoParser.Yield_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#yield_stmt.
    def exitYield_stmt(self, ctx:TaquitoParser.Yield_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#raise_stmt.
    def enterRaise_stmt(self, ctx:TaquitoParser.Raise_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#raise_stmt.
    def exitRaise_stmt(self, ctx:TaquitoParser.Raise_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#import_stmt.
    def enterImport_stmt(self, ctx:TaquitoParser.Import_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#import_stmt.
    def exitImport_stmt(self, ctx:TaquitoParser.Import_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#import_name.
    def enterImport_name(self, ctx:TaquitoParser.Import_nameContext):
        pass

    # Exit a parse tree produced by TaquitoParser#import_name.
    def exitImport_name(self, ctx:TaquitoParser.Import_nameContext):
        pass


    # Enter a parse tree produced by TaquitoParser#import_from.
    def enterImport_from(self, ctx:TaquitoParser.Import_fromContext):
        pass

    # Exit a parse tree produced by TaquitoParser#import_from.
    def exitImport_from(self, ctx:TaquitoParser.Import_fromContext):
        pass


    # Enter a parse tree produced by TaquitoParser#import_as_name.
    def enterImport_as_name(self, ctx:TaquitoParser.Import_as_nameContext):
        pass

    # Exit a parse tree produced by TaquitoParser#import_as_name.
    def exitImport_as_name(self, ctx:TaquitoParser.Import_as_nameContext):
        pass


    # Enter a parse tree produced by TaquitoParser#dotted_as_name.
    def enterDotted_as_name(self, ctx:TaquitoParser.Dotted_as_nameContext):
        pass

    # Exit a parse tree produced by TaquitoParser#dotted_as_name.
    def exitDotted_as_name(self, ctx:TaquitoParser.Dotted_as_nameContext):
        pass


    # Enter a parse tree produced by TaquitoParser#import_as_names.
    def enterImport_as_names(self, ctx:TaquitoParser.Import_as_namesContext):
        pass

    # Exit a parse tree produced by TaquitoParser#import_as_names.
    def exitImport_as_names(self, ctx:TaquitoParser.Import_as_namesContext):
        pass


    # Enter a parse tree produced by TaquitoParser#dotted_as_names.
    def enterDotted_as_names(self, ctx:TaquitoParser.Dotted_as_namesContext):
        pass

    # Exit a parse tree produced by TaquitoParser#dotted_as_names.
    def exitDotted_as_names(self, ctx:TaquitoParser.Dotted_as_namesContext):
        pass


    # Enter a parse tree produced by TaquitoParser#dotted_name.
    def enterDotted_name(self, ctx:TaquitoParser.Dotted_nameContext):
        pass

    # Exit a parse tree produced by TaquitoParser#dotted_name.
    def exitDotted_name(self, ctx:TaquitoParser.Dotted_nameContext):
        pass


    # Enter a parse tree produced by TaquitoParser#global_stmt.
    def enterGlobal_stmt(self, ctx:TaquitoParser.Global_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#global_stmt.
    def exitGlobal_stmt(self, ctx:TaquitoParser.Global_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#nonlocal_stmt.
    def enterNonlocal_stmt(self, ctx:TaquitoParser.Nonlocal_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#nonlocal_stmt.
    def exitNonlocal_stmt(self, ctx:TaquitoParser.Nonlocal_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#assert_stmt.
    def enterAssert_stmt(self, ctx:TaquitoParser.Assert_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#assert_stmt.
    def exitAssert_stmt(self, ctx:TaquitoParser.Assert_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#compound_stmt.
    def enterCompound_stmt(self, ctx:TaquitoParser.Compound_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#compound_stmt.
    def exitCompound_stmt(self, ctx:TaquitoParser.Compound_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#if_stmt.
    def enterIf_stmt(self, ctx:TaquitoParser.If_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#if_stmt.
    def exitIf_stmt(self, ctx:TaquitoParser.If_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#while_stmt.
    def enterWhile_stmt(self, ctx:TaquitoParser.While_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#while_stmt.
    def exitWhile_stmt(self, ctx:TaquitoParser.While_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#for_stmt.
    def enterFor_stmt(self, ctx:TaquitoParser.For_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#for_stmt.
    def exitFor_stmt(self, ctx:TaquitoParser.For_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#try_stmt.
    def enterTry_stmt(self, ctx:TaquitoParser.Try_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#try_stmt.
    def exitTry_stmt(self, ctx:TaquitoParser.Try_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#with_stmt.
    def enterWith_stmt(self, ctx:TaquitoParser.With_stmtContext):
        pass

    # Exit a parse tree produced by TaquitoParser#with_stmt.
    def exitWith_stmt(self, ctx:TaquitoParser.With_stmtContext):
        pass


    # Enter a parse tree produced by TaquitoParser#with_item.
    def enterWith_item(self, ctx:TaquitoParser.With_itemContext):
        pass

    # Exit a parse tree produced by TaquitoParser#with_item.
    def exitWith_item(self, ctx:TaquitoParser.With_itemContext):
        pass


    # Enter a parse tree produced by TaquitoParser#except_clause.
    def enterExcept_clause(self, ctx:TaquitoParser.Except_clauseContext):
        pass

    # Exit a parse tree produced by TaquitoParser#except_clause.
    def exitExcept_clause(self, ctx:TaquitoParser.Except_clauseContext):
        pass


    # Enter a parse tree produced by TaquitoParser#suite.
    def enterSuite(self, ctx:TaquitoParser.SuiteContext):
        pass

    # Exit a parse tree produced by TaquitoParser#suite.
    def exitSuite(self, ctx:TaquitoParser.SuiteContext):
        pass


    # Enter a parse tree produced by TaquitoParser#test.
    def enterTest(self, ctx:TaquitoParser.TestContext):
        pass

    # Exit a parse tree produced by TaquitoParser#test.
    def exitTest(self, ctx:TaquitoParser.TestContext):
        pass


    # Enter a parse tree produced by TaquitoParser#test_nocond.
    def enterTest_nocond(self, ctx:TaquitoParser.Test_nocondContext):
        pass

    # Exit a parse tree produced by TaquitoParser#test_nocond.
    def exitTest_nocond(self, ctx:TaquitoParser.Test_nocondContext):
        pass


    # Enter a parse tree produced by TaquitoParser#lambdef.
    def enterLambdef(self, ctx:TaquitoParser.LambdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#lambdef.
    def exitLambdef(self, ctx:TaquitoParser.LambdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#lambdef_nocond.
    def enterLambdef_nocond(self, ctx:TaquitoParser.Lambdef_nocondContext):
        pass

    # Exit a parse tree produced by TaquitoParser#lambdef_nocond.
    def exitLambdef_nocond(self, ctx:TaquitoParser.Lambdef_nocondContext):
        pass


    # Enter a parse tree produced by TaquitoParser#or_test.
    def enterOr_test(self, ctx:TaquitoParser.Or_testContext):
        pass

    # Exit a parse tree produced by TaquitoParser#or_test.
    def exitOr_test(self, ctx:TaquitoParser.Or_testContext):
        pass


    # Enter a parse tree produced by TaquitoParser#and_test.
    def enterAnd_test(self, ctx:TaquitoParser.And_testContext):
        pass

    # Exit a parse tree produced by TaquitoParser#and_test.
    def exitAnd_test(self, ctx:TaquitoParser.And_testContext):
        pass


    # Enter a parse tree produced by TaquitoParser#not_test.
    def enterNot_test(self, ctx:TaquitoParser.Not_testContext):
        pass

    # Exit a parse tree produced by TaquitoParser#not_test.
    def exitNot_test(self, ctx:TaquitoParser.Not_testContext):
        pass


    # Enter a parse tree produced by TaquitoParser#comparison.
    def enterComparison(self, ctx:TaquitoParser.ComparisonContext):
        pass

    # Exit a parse tree produced by TaquitoParser#comparison.
    def exitComparison(self, ctx:TaquitoParser.ComparisonContext):
        pass


    # Enter a parse tree produced by TaquitoParser#comp_op.
    def enterComp_op(self, ctx:TaquitoParser.Comp_opContext):
        pass

    # Exit a parse tree produced by TaquitoParser#comp_op.
    def exitComp_op(self, ctx:TaquitoParser.Comp_opContext):
        pass


    # Enter a parse tree produced by TaquitoParser#star_expr.
    def enterStar_expr(self, ctx:TaquitoParser.Star_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#star_expr.
    def exitStar_expr(self, ctx:TaquitoParser.Star_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#expr.
    def enterExpr(self, ctx:TaquitoParser.ExprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#expr.
    def exitExpr(self, ctx:TaquitoParser.ExprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#xor_expr.
    def enterXor_expr(self, ctx:TaquitoParser.Xor_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#xor_expr.
    def exitXor_expr(self, ctx:TaquitoParser.Xor_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#and_expr.
    def enterAnd_expr(self, ctx:TaquitoParser.And_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#and_expr.
    def exitAnd_expr(self, ctx:TaquitoParser.And_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#shift_expr.
    def enterShift_expr(self, ctx:TaquitoParser.Shift_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#shift_expr.
    def exitShift_expr(self, ctx:TaquitoParser.Shift_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#arith_expr.
    def enterArith_expr(self, ctx:TaquitoParser.Arith_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#arith_expr.
    def exitArith_expr(self, ctx:TaquitoParser.Arith_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#term.
    def enterTerm(self, ctx:TaquitoParser.TermContext):
        pass

    # Exit a parse tree produced by TaquitoParser#term.
    def exitTerm(self, ctx:TaquitoParser.TermContext):
        pass


    # Enter a parse tree produced by TaquitoParser#factor.
    def enterFactor(self, ctx:TaquitoParser.FactorContext):
        pass

    # Exit a parse tree produced by TaquitoParser#factor.
    def exitFactor(self, ctx:TaquitoParser.FactorContext):
        pass


    # Enter a parse tree produced by TaquitoParser#power.
    def enterPower(self, ctx:TaquitoParser.PowerContext):
        pass

    # Exit a parse tree produced by TaquitoParser#power.
    def exitPower(self, ctx:TaquitoParser.PowerContext):
        pass


    # Enter a parse tree produced by TaquitoParser#atom.
    def enterAtom(self, ctx:TaquitoParser.AtomContext):
        pass

    # Exit a parse tree produced by TaquitoParser#atom.
    def exitAtom(self, ctx:TaquitoParser.AtomContext):
        pass


    # Enter a parse tree produced by TaquitoParser#testlist_comp.
    def enterTestlist_comp(self, ctx:TaquitoParser.Testlist_compContext):
        pass

    # Exit a parse tree produced by TaquitoParser#testlist_comp.
    def exitTestlist_comp(self, ctx:TaquitoParser.Testlist_compContext):
        pass


    # Enter a parse tree produced by TaquitoParser#trailer.
    def enterTrailer(self, ctx:TaquitoParser.TrailerContext):
        pass

    # Exit a parse tree produced by TaquitoParser#trailer.
    def exitTrailer(self, ctx:TaquitoParser.TrailerContext):
        pass


    # Enter a parse tree produced by TaquitoParser#subscriptlist.
    def enterSubscriptlist(self, ctx:TaquitoParser.SubscriptlistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#subscriptlist.
    def exitSubscriptlist(self, ctx:TaquitoParser.SubscriptlistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#subscript.
    def enterSubscript(self, ctx:TaquitoParser.SubscriptContext):
        pass

    # Exit a parse tree produced by TaquitoParser#subscript.
    def exitSubscript(self, ctx:TaquitoParser.SubscriptContext):
        pass


    # Enter a parse tree produced by TaquitoParser#sliceop.
    def enterSliceop(self, ctx:TaquitoParser.SliceopContext):
        pass

    # Exit a parse tree produced by TaquitoParser#sliceop.
    def exitSliceop(self, ctx:TaquitoParser.SliceopContext):
        pass


    # Enter a parse tree produced by TaquitoParser#exprlist.
    def enterExprlist(self, ctx:TaquitoParser.ExprlistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#exprlist.
    def exitExprlist(self, ctx:TaquitoParser.ExprlistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#testlist.
    def enterTestlist(self, ctx:TaquitoParser.TestlistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#testlist.
    def exitTestlist(self, ctx:TaquitoParser.TestlistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#dictorsetmaker.
    def enterDictorsetmaker(self, ctx:TaquitoParser.DictorsetmakerContext):
        pass

    # Exit a parse tree produced by TaquitoParser#dictorsetmaker.
    def exitDictorsetmaker(self, ctx:TaquitoParser.DictorsetmakerContext):
        pass


    # Enter a parse tree produced by TaquitoParser#classdef.
    def enterClassdef(self, ctx:TaquitoParser.ClassdefContext):
        pass

    # Exit a parse tree produced by TaquitoParser#classdef.
    def exitClassdef(self, ctx:TaquitoParser.ClassdefContext):
        pass


    # Enter a parse tree produced by TaquitoParser#arglist.
    def enterArglist(self, ctx:TaquitoParser.ArglistContext):
        pass

    # Exit a parse tree produced by TaquitoParser#arglist.
    def exitArglist(self, ctx:TaquitoParser.ArglistContext):
        pass


    # Enter a parse tree produced by TaquitoParser#argument.
    def enterArgument(self, ctx:TaquitoParser.ArgumentContext):
        pass

    # Exit a parse tree produced by TaquitoParser#argument.
    def exitArgument(self, ctx:TaquitoParser.ArgumentContext):
        pass


    # Enter a parse tree produced by TaquitoParser#comp_iter.
    def enterComp_iter(self, ctx:TaquitoParser.Comp_iterContext):
        pass

    # Exit a parse tree produced by TaquitoParser#comp_iter.
    def exitComp_iter(self, ctx:TaquitoParser.Comp_iterContext):
        pass


    # Enter a parse tree produced by TaquitoParser#comp_for.
    def enterComp_for(self, ctx:TaquitoParser.Comp_forContext):
        pass

    # Exit a parse tree produced by TaquitoParser#comp_for.
    def exitComp_for(self, ctx:TaquitoParser.Comp_forContext):
        pass


    # Enter a parse tree produced by TaquitoParser#comp_if.
    def enterComp_if(self, ctx:TaquitoParser.Comp_ifContext):
        pass

    # Exit a parse tree produced by TaquitoParser#comp_if.
    def exitComp_if(self, ctx:TaquitoParser.Comp_ifContext):
        pass


    # Enter a parse tree produced by TaquitoParser#yield_expr.
    def enterYield_expr(self, ctx:TaquitoParser.Yield_exprContext):
        pass

    # Exit a parse tree produced by TaquitoParser#yield_expr.
    def exitYield_expr(self, ctx:TaquitoParser.Yield_exprContext):
        pass


    # Enter a parse tree produced by TaquitoParser#yield_arg.
    def enterYield_arg(self, ctx:TaquitoParser.Yield_argContext):
        pass

    # Exit a parse tree produced by TaquitoParser#yield_arg.
    def exitYield_arg(self, ctx:TaquitoParser.Yield_argContext):
        pass


    # Enter a parse tree produced by TaquitoParser#strr.
    def enterStrr(self, ctx:TaquitoParser.StrrContext):
        pass

    # Exit a parse tree produced by TaquitoParser#strr.
    def exitStrr(self, ctx:TaquitoParser.StrrContext):
        pass


    # Enter a parse tree produced by TaquitoParser#number.
    def enterNumber(self, ctx:TaquitoParser.NumberContext):
        pass

    # Exit a parse tree produced by TaquitoParser#number.
    def exitNumber(self, ctx:TaquitoParser.NumberContext):
        pass


    # Enter a parse tree produced by TaquitoParser#integer.
    def enterInteger(self, ctx:TaquitoParser.IntegerContext):
        pass

    # Exit a parse tree produced by TaquitoParser#integer.
    def exitInteger(self, ctx:TaquitoParser.IntegerContext):
        pass


