from __future__ import print_function
import io
import os
from setuptools import setup, find_packages

import taquito

here = os.path.abspath(os.path.dirname(__file__))


def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

long_description = read('README.rst', 'CHANGELOG.rst')

setup(
    name='taquito',
    version=taquito.__version__,
    packages=find_packages(),
    include_package_data=True,
    package_data={'taquito': ['CONTRIB.rst', 'CHANGELOG.rst']},
    # Metadata
    author='Horacio Hoyos Rodriguez',
    author_email='arcanefoam@gmail.com',
    description='A textual syntax for pyMOF and pyEcore metamodels',
    license='Apache Software License',
    url='https://bitbucket.org/arcanefoam/taquito',
    long_description=long_description,
    platforms='any',
    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 0 - Alpha',
        'Natural Language :: English',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)