taquito
=======

Taquito is a textual syntax for pyMOF and pyEcore metamodels. Taquito extends Python, which results in a fully fledged
programming language. You can use it not only to specify the structure of your metamodel, but also the behavior of your
operations and derived features as well as the conversion logic of your data types. 

Contributing
____________

Please read CONTRIBUTING for details on our code of conduct, and the process for submitting pull requests to us.

License
_______

This project is licensed under the Apache 2.0 License - see the LICENSE file for details