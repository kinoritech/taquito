Changelog
=========
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[Unreleased]
------------

Added
~~~~~
- Add tests to improve coverage.
- Completed the README, added example


[0.0.1] - 2017-??-??
--------------------
Initial release. Includes the first API implementation, tests and documentation.

.. Added
   ~~~~~
   Changed
   ~~~~~~~
   Fixed
   ~~~~~
   Removed
   ~~~~~~~
